using System.Collections.Generic;
using OrangeShotStudio.Analytics;
using OrangeShotStudio.Application;
using OrangeShotStudio.CarDrift.View.UI;
using OrangeShotStudio.DB.Application;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;
using UnityEngine;

namespace BaseCode.Scripts
{
    public class ApplicationNameLauncher : BaseLauncher
    {
        private readonly ICompositionRoot _cr = new ApplicationNameCompositionRoot();

        protected override ICompositionRoot Cr => _cr;
    }

    public class ApplicationNameCompositionRoot : ICompositionRoot
    {
        public void Launch()
        {
            var prefabProvider = new PrefabProvider();
            var windowsFactory = new ApplicationNameWindowsFactory(prefabProvider);
            var interfaceView = new InterfaceView(prefabProvider, windowsFactory);
            var stateFactory = new ApplicationNameStateFactory(prefabProvider, interfaceView);
            var analyticManager = new AnalyticManager();
            new ApplicationNameController(stateFactory, interfaceView, analyticManager);
        }
    }

    public class ApplicationNameController : BaseGameController
    {
        private readonly InterfaceView _interfaceView;

        public ApplicationNameController(ApplicationNameStateFactory ApplicationNameStateFactory,
            InterfaceView interfaceView, IAnalyticsService analyticsService) : base(analyticsService)
        {
            _interfaceView = interfaceView;
        }

        protected override void PreStateChange()
        {
            _interfaceView.HideAll();
        }
    }

    public class ApplicationNameWindowsFactory : IWindowsFactory
    {
        private readonly IPrefabProvider _prefabProvider;

        public ApplicationNameWindowsFactory(IPrefabProvider prefabProvider)
        {
            _prefabProvider = prefabProvider;
        }

        public List<IWindow> CreateAllWindows()
        {
            throw new System.NotImplementedException();
        }
    }

    public partial class ApplicationNameStateFactory : BaseGameStateFactory
    {
        public ApplicationNameStateFactory(IPrefabProvider prefabProvider, InterfaceView interfaceView) : base(
            prefabProvider,
            interfaceView)
        {
        }
    }

    public class GameStateNameState : BaseGameState
    {
        private readonly InterfaceView _interfaceView;

        public GameStateNameState(InterfaceView interfaceView)
        {
            _interfaceView = interfaceView;
        }
    }

    public partial class ApplicationNameStateFactory : BaseGameStateFactory
    {
        public IGameState CreateGameStateNameState()
        {
            return new GameStateNameState(InterfaceView);
        }
    }

    public class WindowNameWindow : BaseWindow<WindowNameWidget>, IWindowNameWindow
    {
        public WindowNameWindow(IPrefabProvider prefabProvider) : base(prefabProvider, "WindowNameWindow")
        {
        }
    }

    public interface IWindowNameWindow : IWindow
    {
    }

    public class WindowNameWidget : MonoBehaviour
    {
    }
}