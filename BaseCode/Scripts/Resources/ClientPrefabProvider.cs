using System.Linq;
using OrangeShotStudio.Data;
using UnityEditor;
using UnityEngine;

namespace OrangeShotStudio.Provider
{
    public class ClientPrefabProvider : ComplexPrefabProviderBase
    {
        public ClientPrefabProvider() : base(1)
        {
        }
    }
}