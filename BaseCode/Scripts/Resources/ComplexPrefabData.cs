using System;
using Assets.Scripts.Model;
using UnityEngine;

namespace OrangeShotStudio.Data
{
    [Serializable]
    public class ComplexPrefabData : IDefinition
    {
        public string Id;
        public GameObject[] Prefabs;
        string IDefinition.Id { get { return Id; } }
    }
}