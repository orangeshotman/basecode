using System;
using Assets.Scripts.Model;
using UnityEngine;

namespace OrangeShotStudio.Data
{
    [Serializable]
    public class PrefabData : IDefinition
    {
        public string Id;
        public GameObject Prefab;
        string IDefinition.Id { get { return Id; } }
    }
}