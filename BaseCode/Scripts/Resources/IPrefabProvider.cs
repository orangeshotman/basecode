﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace OrangeShotStudio.Provider
{
    public interface IPrefabProvider
    {
        GameObject GetCanvasPrefab { get; }
        GameObject GetPrefab(string id);
    }
    
    public interface IAsyncPrefabProvider
    {
        Task<GameObject> GetPrefabAsync(string id);
        AsyncOperationHandle<GameObject> GetPrefab(string id);
    }
}
