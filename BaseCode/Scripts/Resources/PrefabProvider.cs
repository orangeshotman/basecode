﻿using System.Linq;
using OrangeShotStudio.Data;
using UnityEngine;

namespace OrangeShotStudio.Provider
{
    public class PrefabProvider : IPrefabProvider
    {
        private PrefabDb _prefabDb;

        public PrefabProvider()
        {
            _prefabDb = Resources.Load<PrefabDb>("PrefabDb");
        }

        public GameObject GetCanvasPrefab
        {
            get { return _prefabDb.Canvas; }
        }

        public GameObject GetPrefab(string id)
        {
            var firstOrDefault = _prefabDb.Data.FirstOrDefault(x=>x.Id == id);
            if (firstOrDefault != null)
                return firstOrDefault.Prefab;
            Debug.LogError(id + " not found");
            return null;
        }
    }
}