using System.Linq;
using UnityEngine;

namespace OrangeShotStudio.Data
{
    [CreateAssetMenu(fileName = "PrefabDb", menuName = "PrefabDb")]
    public class PrefabDb : BaseDb<PrefabData>
    {
        public GameObject Canvas;
    }
}