using UnityEngine;

namespace OrangeShotStudio.Data
{
    [CreateAssetMenu(fileName = "ComplexPrefabDb", menuName = "ComplexPrefabDb")]
    public class ComplexPrefabDb : BaseDb<ComplexPrefabData>
    {
        public GameObject Canvas;
    }
}