using OrangeShotStudio.Data;
using UnityEngine;

namespace OrangeShotStudio.Provider
{
    public class ComplexPrefabProviderBase : IPrefabProvider
    {
        private readonly int _index;
        private ComplexPrefabDb _complexPrefabDb;
        public GameObject GetCanvasPrefab => _complexPrefabDb.Canvas;
        public ComplexPrefabProviderBase(int index)
        {
            _index = index;
            _complexPrefabDb = Resources.Load<ComplexPrefabDb>("ComplexPrefabDb");
        }

        public GameObject GetPrefab(string id)
        {
            ComplexPrefabData result = null;
            for (var index = 0; index < _complexPrefabDb.Data.Count; index++)
            {
                var x = _complexPrefabDb.Data[index];
                if (x.Id == id)
                {
                    result = x;
                    break;
                }
            }

            return result?.Prefabs[_index];
        }
    }
}