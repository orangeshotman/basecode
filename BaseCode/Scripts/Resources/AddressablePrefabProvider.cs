using System;
using UnityEngine;

namespace OrangeShotStudio.Provider
{
    public class AddressablePrefabProvider : IPrefabProvider
    {
        private IAsyncPrefabProvider _asyncPrefabProvider;

        public AddressablePrefabProvider()
        {
            _asyncPrefabProvider = new AddressableAsyncPrefabProvider();
        }

        public GameObject GetCanvasPrefab => GetPrefab("Canvas");

        public GameObject GetPrefab(string id)
        {
            var result = _asyncPrefabProvider.GetPrefabAsync(id);
            var awaiter = result.GetAwaiter();
            result.Wait(TimeSpan.FromSeconds(1));
            

            if (result.IsCompleted)
                return result.Result;
            if (result.IsCanceled)
            {
                Debug.LogError(id + " was canceled");
                return null;
            }

            if (result.IsFaulted)
            {
                Debug.LogException(result.Exception);
                return null;
            }
            
            Debug.LogError(id + " PROBLEM");
            return null;
        }
    }
}