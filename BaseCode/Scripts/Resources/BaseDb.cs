using System.Collections.Generic;
using Assets.Scripts.Model;
using UnityEngine;

namespace OrangeShotStudio.Data
{
    public abstract class BaseDb<TDefinition> : ScriptableObject, IScriptableDatabase<TDefinition> where TDefinition : class, IDefinition
    {
        public List<TDefinition> PrefabData;

        public List<TDefinition> Data
        {
            get { return PrefabData; }
        }
    }
}