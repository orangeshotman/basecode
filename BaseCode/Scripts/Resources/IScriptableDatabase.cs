using System.Collections.Generic;

namespace OrangeShotStudio.Data
{
    public interface IScriptableDatabase<T> where T : class 
    {
        List<T> Data { get; }
    }
}