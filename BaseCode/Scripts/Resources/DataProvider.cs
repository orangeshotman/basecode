﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Model;
using OrangeShotStudio.Data;
using UnityEngine;

namespace OrangeShotStudio.Provider
{
    public abstract class DataProvider<TScriptable, TData> where TData: class, IDefinition where TScriptable : ScriptableObject, IScriptableDatabase<TData>
    {
        protected readonly TScriptable Db;

        protected DataProvider(string id)
        {
            Db = Resources.Load<TScriptable>(id);
        }

        public TData GetData(string id)
        {
            return  Db.Data.FirstOrDefault(x=>x.Id == id);
        }

        public List<TData> GetAllData()
        {
            return Db.Data;
        }


    }
}