using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace OrangeShotStudio.Provider
{
    public class AddressableAsyncPrefabProvider : IAsyncPrefabProvider
    {
        public Task<GameObject> GetPrefabAsync(string id)
        {
            return Addressables.LoadAssetAsync<GameObject>(id).Task;
        }

        public AsyncOperationHandle<GameObject> GetPrefab(string id)
        {
            var result = Addressables.LoadAssetAsync<GameObject>(id);
            return result;
        }
    }
}