using OrangeShotStudio.View;

namespace BaseCode.Scripts.Templates.Controller
{
    public interface ISimpleMainMenuWindow : IWindow
    {
        bool Start { get; }
    }
}