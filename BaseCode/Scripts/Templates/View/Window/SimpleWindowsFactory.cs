using System.Collections.Generic;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;

namespace BaseCode.Scripts.Templates.Controller
{
    public class SimpleWindowsFactory : IWindowsFactory
    {
        private readonly IPrefabProvider _prefabProvider;

        public SimpleWindowsFactory(IPrefabProvider prefabProvider)
        {
            _prefabProvider = prefabProvider;
        }
        public List<IWindow> CreateAllWindows()
        {
            var result = new List<IWindow>();
            result.Add(new SimpleMainMenuWindow(_prefabProvider));
            return result;
        }
    }
}