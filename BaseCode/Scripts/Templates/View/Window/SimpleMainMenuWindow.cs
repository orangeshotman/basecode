using OrangeShotStudio.CarDrift.View.UI;
using OrangeShotStudio.Provider;

namespace BaseCode.Scripts.Templates.Controller
{
    public class SimpleMainMenuWindow : BaseWindow<SimpleMainMenuWidget>, ISimpleMainMenuWindow
    {
        public bool Start => Widget.Start.Pushed;
        public SimpleMainMenuWindow(IPrefabProvider prefabProvider) : base(prefabProvider, "SimpleMainMenuWindow")
        {
        }
    }
}