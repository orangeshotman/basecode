using OrangeShotStudio.View;
using UnityEngine;

namespace BaseCode.Scripts.Templates.Controller
{
    public interface IDragControlWindow : IWindow
    {
        bool GetPushPosition(out Vector2 pos);
    }
}