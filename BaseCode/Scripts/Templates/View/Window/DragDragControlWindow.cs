using BaseCode.Scripts.Templates.Behaviours;
using OrangeShotStudio.CarDrift.View.UI;
using OrangeShotStudio.Provider;
using UnityEngine;

namespace BaseCode.Scripts.Templates.Controller
{
    public class DragDragControlWindow : BaseWindow<DragControlWidget>, IDragControlWindow
    {
        public DragDragControlWindow(IPrefabProvider prefabProvider) : base(prefabProvider, "DragControlWindow")
        {
        }

        public bool GetPushPosition(out Vector2 pos)
        {
            pos = Vector2.zero;
            if (Widget.DragControlButton.Pushed)
            {
                pos = Widget.DragControlButton.Position;
                return true;
            }
            return false;
        }
    }
}