using OrangeShotStudio.Application;

namespace BaseCode.Scripts.Templates.Controller
{
    public class SimpleApplicationLauncher : BaseLauncher
    {
        private readonly ICompositionRoot _cr = new SimpleCompositionRoot();

        protected override ICompositionRoot Cr => _cr;
    }
}