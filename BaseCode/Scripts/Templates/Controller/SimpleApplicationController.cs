﻿using OrangeShotStudio.Analytics;
using OrangeShotStudio.Application;
using OrangeShotStudio.View;

namespace BaseCode.Scripts.Templates.Controller
{
    public class SimpleApplicationController : BaseGameController
    {
        private readonly InterfaceView _interfaceView;

        public SimpleApplicationController(SimpleStateFactory simpleStateFactory, InterfaceView interfaceView,
            IAnalyticsService analyticsService) : base(analyticsService)
        {
            _interfaceView = interfaceView;
            GameState = simpleStateFactory.SimpleMainMenuState();
            GameState.Initialize();
        }

        protected override void PreStateChange()
        {
            _interfaceView.HideAll();
        }
    }
}