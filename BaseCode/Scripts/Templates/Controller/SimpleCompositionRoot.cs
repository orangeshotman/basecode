using OrangeShotStudio.Analytics;
using OrangeShotStudio.Application;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;

namespace BaseCode.Scripts.Templates.Controller
{
    public class SimpleCompositionRoot : ICompositionRoot
    {
        public void Launch()
        {
            var prefabProvider = new PrefabProvider();
            var windowsFactory = new SimpleWindowsFactory(prefabProvider);
            var interfaceView = new InterfaceView(prefabProvider, windowsFactory);
            var stateFactory = new SimpleStateFactory(prefabProvider, interfaceView);
            var analyticManager = new AnalyticManager();
            new SimpleApplicationController(stateFactory, interfaceView, analyticManager);
        }
    }
}