using OrangeShotStudio.Application;
using OrangeShotStudio.DB.Application;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;

namespace BaseCode.Scripts.Templates.Controller
{
    public class SimpleMainMenuState : BaseGameState
    {
        private readonly InterfaceView _interfaceView;

        public SimpleMainMenuState(InterfaceView interfaceView)
        {
            _interfaceView = interfaceView;
        }

        public override void Initialize()
        {
            var mainMenu = _interfaceView.GetWindow<ISimpleMainMenuWindow>();
            mainMenu.Show();
        }
    }
    
    public partial class SimpleStateFactory : BaseGameStateFactory
    {
        public IGameState SimpleMainMenuState()
        {
            return new SimpleMainMenuState(InterfaceView);
        }
    }
}