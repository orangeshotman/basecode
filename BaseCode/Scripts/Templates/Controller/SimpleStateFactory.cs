using OrangeShotStudio.Application;
using OrangeShotStudio.DB.Application;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;

namespace BaseCode.Scripts.Templates.Controller
{
    public partial class SimpleStateFactory : BaseGameStateFactory
    {
        public SimpleStateFactory(IPrefabProvider prefabProvider, InterfaceView interfaceView) : base(prefabProvider, interfaceView)
        {
        }
    }
}