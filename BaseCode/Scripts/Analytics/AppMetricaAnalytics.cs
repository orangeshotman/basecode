using System.Collections.Generic;
using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    public class AppMetricaAnalytics : IAnalyticsService
    {
        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
#if APPMETRICA_ANALYTICS
            string messageName = eventName == ProgressEventName.Start ? "level_start" : "level_finish";
            var dic = new Dictionary<string, object>()
            {
                { "level_number", int.Parse(levelName) }
            };
            if(score.HasValue && eventName != ProgressEventName.Start)
                dic.Add("progress", Mathf.Clamp(0,100, score.Value)); 
            if(eventName != ProgressEventName.Start)
                dic.Add("result", eventName == ProgressEventName.Complete ? "win":"lose");
            AppMetrica.Instance.ReportEvent(messageName, dic);
            AppMetrica.Instance.SendEventsBuffer();
#endif
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
#if APPMETRICA_ANALYTICS
            AppMetrica.Instance.ReportEvent(eventName, parameters);
#endif
        }

        public void OnApplicationPause(bool pauseStatus)
        {
        }

        public void OnApplicationExit()
        {
        }

        public void ShowDebug()
        {
        }
    }
}