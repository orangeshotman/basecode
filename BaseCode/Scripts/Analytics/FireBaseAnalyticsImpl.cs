using System.Collections.Generic;
#if FIREBASE_ANALYTICS
using Firebase.Analytics;
using UnityEngine;
#endif

namespace OrangeShotStudio.Analytics
{
    public class FireBaseAnalyticsImpl : IAnalyticsService
    {
        public FireBaseAnalyticsImpl()
        {
#if FIREBASE_ANALYTICS && !UNITY_EDITOR
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available) {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    //   app = Firebase.FirebaseApp.DefaultInstance;
                    FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                } else {
                    UnityEngine.Debug.LogError(System.String.Format(
                        "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
#endif
        }
        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
#if FIREBASE_ANALYTICS && !UNITY_EDITOR
            switch (eventName)
            {
                case ProgressEventName.Start:
                    FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, "LevelName", levelName);
                    break;
                case ProgressEventName.Complete:
                    FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd, "LevelName", levelName);
                    break;
                case ProgressEventName.Fail:
                    FirebaseAnalytics.LogEvent("Fail", "LevelName", levelName);
                    break;
            }
#endif
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
#if FIREBASE_ANALYTICS && !UNITY_EDITOR
            var par = new List<Parameter>();
            foreach (var parameter in parameters)
            {
                if (parameter.Value is int intValue)
                {
                    par.Add(new Parameter(parameter.Key, intValue));
                }

                else if (parameter.Value is long longValue)
                {
                    par.Add(new Parameter(parameter.Key, longValue));
                }

                else if (parameter.Value is string stringValue)
                {
                    par.Add(new Parameter(parameter.Key, stringValue));
                }

                else if (parameter.Value is double doubleValue)
                {
                    par.Add(new Parameter(parameter.Key, doubleValue));
                }

                else if (parameter.Value is float floatValue)
                {
                    par.Add(new Parameter(parameter.Key, floatValue));
                }
            }

            FirebaseAnalytics.LogEvent(eventName, par.ToArray());
#endif
        }

        public void OnApplicationPause(bool pauseStatus)
        {
        }

        public void OnApplicationExit()
        {
            
        }

        public void ShowDebug()
        {
            
        }
    }
}