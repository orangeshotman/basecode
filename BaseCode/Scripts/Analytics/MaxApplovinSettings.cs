using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    [CreateAssetMenu(fileName = "MaxApplovinSettings", menuName = "Services/MaxApplovinSettings")]
    public class MaxApplovinSettings : ScriptableObject
    {
        public string SdkKey;
    }
}