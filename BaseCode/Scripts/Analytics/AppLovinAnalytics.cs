using System.Collections.Generic;
using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    public class AppLovinAnalytics : IAnalyticsService
    {
        public AppLovinAnalytics()
        {
#if MAXAPPLOVIN
            var settings = MaxApplovinSettingsProvider.GetSettings();
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                Debug.Log("AppLovinAnalytics has been initialized");
            };

            MaxSdk.SetSdkKey(settings.SdkKey);
            MaxSdk.InitializeSdk();
#endif
        }

        public void ShowDebug()
        {
#if MAXAPPLOVIN
            if (MaxSdk.IsInitialized())
                MaxSdk.ShowMediationDebugger();
#endif
        }


        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
        }

        public void OnApplicationPause(bool pauseStatus)
        {
        }

        public void OnApplicationExit()
        {
        }
    }
}