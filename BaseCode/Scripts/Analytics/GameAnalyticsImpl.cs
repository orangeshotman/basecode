using System.Collections.Generic;
using UnityEngine;
#if GAMEANALYTICS
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;

#endif

namespace OrangeShotStudio.Analytics
{
    public class GameAnalyticsImpl : IAnalyticsService
    {
        public GameAnalyticsImpl()
        {
#if GAMEANALYTICS
            GameAnalytics.Initialize();
#endif
        }

        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
#if GAMEANALYTICS

            if (!score.HasValue)
            {
                switch (eventName)
                {
                    case ProgressEventName.Complete:
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, levelName);
                        break;
                    case ProgressEventName.Fail:
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, levelName);
                        break;
                    case ProgressEventName.Start:
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, levelName);
                        break;
                }
            }
            else
            {
                switch (eventName)
                {
                    case ProgressEventName.Complete:
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, levelName, score.Value);
                        break;
                    case ProgressEventName.Fail:
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, levelName, score.Value);
                        break;
                    case ProgressEventName.Start:
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, levelName, score.Value);
                        break;
                }
            }

#endif
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
#if GAMEANALYTICS
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    if (parameter.Value is float parameterValue)
                    {
                        string gaEventName = parameter.Key;
                        float gaValue = parameterValue;
                        GameAnalytics.NewDesignEvent(gaEventName, parameterValue);
                    }
                }
            }
            else
            {
                GameAnalytics.NewDesignEvent(eventName);
            }
#endif
        }

        public void OnApplicationPause(bool pauseStatus)
        {
        }

        public void OnApplicationExit()
        {
            
        }

        public void ShowDebug()
        {
            
        }
    }
}