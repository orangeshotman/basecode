using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OrangeShotStudio.Data;
#if ORANGE_ANALYTICS
using OrangeShotStudio.Network;
using OrangeShotStudio.Pixockets;
using Pixockets.Pools;
#endif
using OrangeShotStudio.Scripts.Tools;
using UnityEngine;

namespace OrangeShotStudio.Analytics
{
#if ORANGE_ANALYTICS
    public class OrangeShotAnalytics : IAnalyticsService
    {
        private readonly object _lockObject = new object();
        private readonly PrefsPropertyString _randomId = new PrefsPropertyString("RandomId", Guid.NewGuid().ToString());
        private NetworkClient _networkClient;
        private volatile bool _keepWorking = true;
        private float _nextPingTime;
        private BitPacker _bitPacker = new BitPacker();
        private BufferPoolBase _bufferPool = new ByteBufferPool();
        private float _currentTime;
        private User _user;
        private string _ping = JsonConvert.SerializeObject(new Ping());
        private Queue<IAnalyticsMessage> _queueMessage = new Queue<IAnalyticsMessage>();
        private bool _pauseStatus;

        public OrangeShotAnalytics()
        {
            _user = new User(UnityEngine.Application.identifier,
                UnityEngine.Application.version,
                _randomId.Value,
                UnityEngine.Application.platform.ToString(),
                SystemInfo.operatingSystem,
                UnityEngine.Application.systemLanguage.ToString(),
                System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName);
            var ip = IPAddress.Parse("77.37.138.117");
            Debug.Log($"Analytics address {ip}");
            _networkClient = new NetworkClient(ip, 2417, 20000, new UnityLogger());
            _queueMessage.Enqueue(new StartSession(_user, Time.time));
            Process();
        }

        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
            lock (_lockObject)
            {
                _queueMessage.Enqueue(new Progression(_user, Time.time, eventName, levelName, score.HasValue ? score.Value : 0));
            }
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
            var dic= new Dictionary<string, string>();
            foreach (var parameter in parameters)
            {
                dic.Add(parameter.Key, parameter.Value.ToString());
            }
            lock (_lockObject)
            {
                _queueMessage.Enqueue(new CustomEvent(_user, Time.time, eventName, dic));
            }
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            _pauseStatus = pauseStatus;
        }

        public void OnApplicationExit()
        {
            _keepWorking = false;
            
        }

        public void ShowDebug()
        {
            
        }

        private async void Process()
        {
            Thread thread = new Thread(ThreadProcess);
            thread.Start();
            while (_keepWorking)
            {
                lock (_lockObject)
                {
                    _currentTime = Time.time;
                }

                await Task.Yield();
            }
        }

        private void ThreadProcess()
        {
            while (_keepWorking)
            {
                Thread.Sleep(100);
                if(_pauseStatus)
                    continue;
                lock (_lockObject)
                {
                    _networkClient.Update();
                    if (_networkClient.Connection.Connected)
                    {
                        if (_currentTime > _nextPingTime)
                        {
                            _queueMessage.Enqueue(new Ping());
                            _nextPingTime = _currentTime + 5;
                        }

                        while (_queueMessage.Count > 0)
                        {
                            var message = _queueMessage.Dequeue();
                            var buffer = _bufferPool.Get(1024);
                            _bitPacker.Reset(buffer, 0, buffer.Length);
                            _bitPacker.PackSInt32((int) message.MessageType);
                            _bitPacker.PackString(_user.GameId);
                            var json = JsonConvert.SerializeObject(message);
                            _bitPacker.PackString(json);
                            var networkMessage = new NetworkMessage(buffer, _bufferPool);
                            _networkClient.Connection.SendMessage(networkMessage, true);
                            networkMessage.Dispose();
                        }
                    }
                }
            }
            lock (_lockObject)
            {
                _networkClient.Dispose();
                Debug.Log("Analytics connection has been disposed");
            }
        }
    }
#endif

}