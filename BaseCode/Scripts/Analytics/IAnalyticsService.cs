using System.Collections.Generic;

namespace OrangeShotStudio.Analytics
{
    public interface IAnalyticsService
    {
        void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null);
        void SendCustomEvent(string eventName, Dictionary<string, object> parameters);
        void OnApplicationPause(bool pauseStatus);
        void OnApplicationExit();
        void ShowDebug();
    }

    public enum ProgressEventName
    {
        Start,
        Complete,
        Fail,
    }
}
