using System.Collections.Generic;
using OrangeShotStudio.Scripts.Tools;
using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    public class AnalyticManager : IAnalyticsService
    {
        private readonly List<IAnalyticsService> _analyticsServices = new List<IAnalyticsService>();
 
        private bool _testLoop;

        public AnalyticManager()
        {
            _testLoop = ToolsExtensions.IsTestLoop();
            if (!_testLoop)
            {
                _analyticsServices.Add(new UnityAnalytics());
                _analyticsServices.Add(new GameAnalyticsImpl());
                _analyticsServices.Add(new FaceBookAnalytics());
                _analyticsServices.Add(new FireBaseAnalyticsImpl());
                _analyticsServices.Add(new TenjinAnalytics());
                _analyticsServices.Add(new AppLovinAnalytics());
                _analyticsServices.Add(new AppMetricaAnalytics());

#if ORANGE_ANALYTICS
#if UNITY_EDITOR && EDITOR_ORANGE_ANALYTICS
                _orangeShotAnalytics = new OrangeShotAnalytics();
#elif !UNITY_EDITOR
                _orangeShotAnalytics = new OrangeShotAnalytics();
#endif
#endif
            }
        }
        

        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
            if (_testLoop)
                return;
#if UNITY_ANDROID || UNITY_IOS
            foreach (var service in _analyticsServices)
            {
                service.SendProgressEvent(eventName, levelName, score);
            }
#endif
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
            if (_testLoop)
                return;
#if UNITY_ANDROID || UNITY_IOS
            foreach (var service in _analyticsServices)
            {
                service.SendCustomEvent(eventName, parameters);
            }
#endif
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            foreach (var service in _analyticsServices)
            {
                service.OnApplicationPause(pauseStatus);
            }
        }

        public void OnApplicationExit()
        {
            foreach (var service in _analyticsServices)
            {
                service.OnApplicationExit();
            }
        }

        public void ShowDebug()
        {
            if(!Debug.isDebugBuild)
                return;
            foreach (var service in _analyticsServices)
            {
                service.ShowDebug();
            }
        }
    }
}