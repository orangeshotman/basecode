using System.Collections.Generic;
using UnityEngine;

#if FACEBOOKANALYTICS
using Facebook.Unity;
#endif

namespace OrangeShotStudio.Analytics
{
    public class FaceBookAnalytics : IAnalyticsService
    {
        private readonly Dictionary<string, Dictionary<string, object>> _delayedEvents;

        public FaceBookAnalytics()
        {
#if FACEBOOKANALYTICS
#if UNITY_ANDROID || UNITY_IOS 
            _delayedEvents = new Dictionary<string, Dictionary<string, object>>();
            if (!FB.IsInitialized)
                FB.Init(InitCallback);
            else
                FB.ActivateApp();
#endif
#endif

        }


        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
            switch (eventName)
            {
                case ProgressEventName.Start:
                    SendCustomEvent("Start", new Dictionary<string, object>(){{"LevelName", levelName}});
                    break;
                case ProgressEventName.Complete:
                    SendCustomEvent("Complete", new Dictionary<string, object>(){{"LevelName", levelName}});
                    break;
                case ProgressEventName.Fail:
                    SendCustomEvent("Fail", new Dictionary<string, object>(){{"LevelName", levelName}});
                    break;
            }
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
#if FACEBOOKANALYTICS
            if (FB.IsInitialized)
            {
                FB.LogAppEvent(eventName, parameters: parameters);
                foreach (var key in _delayedEvents.Keys)
                {
                    FB.LogAppEvent(key, parameters: _delayedEvents[key]);
                }
                _delayedEvents.Clear();
            }
            else
            {
                _delayedEvents[eventName] = parameters;
            }        
#endif
        }

        public void OnApplicationPause(bool pauseStatus)
        {
#if FACEBOOKANALYTICS
            if (pauseStatus)
                return;
            if (FB.IsInitialized)
                FB.ActivateApp();
            
#endif
        }

        public void OnApplicationExit()
        {
            
        }

        public void ShowDebug()
        {
            
        }

        private void InitCallback ()
        {
#if FACEBOOKANALYTICS
            if (FB.IsInitialized) {
                FB.ActivateApp();
            } else {
                Debug.LogError("Failed to Initialize the Facebook SDK");
            }
#endif
        }
    }
}