using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    public static class MaxApplovinSettingsProvider
    {
        public static MaxApplovinSettings GetSettings()
        {
            return Resources.Load<MaxApplovinSettings>("MaxApplovinSettings");
        }
    }
}