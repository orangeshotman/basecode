using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    [CreateAssetMenu(fileName = "TenjinSettings", menuName = "Services/TenjinSettings")]
    public class TenjinSettings : ScriptableObject
    {
        public string ApiKey;
    }
}