using System.Collections.Generic;

namespace OrangeShotStudio.Analytics
{
    public class TenjinAnalytics : IAnalyticsService
    {
#if TENJIN_ANALYTICS
        private BaseTenjin _instance;
#endif

        public TenjinAnalytics()
        {
            Connect();
        }

        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
#if TENJIN_ANALYTICS
            _instance.SendEvent(eventName + levelName);
#endif
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
#if TENJIN_ANALYTICS
            _instance.SendEvent(eventName);
#endif
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
                Connect();
        }

        public void OnApplicationExit()
        {
        }

        public void ShowDebug()
        {
        }

        private void Connect()
        {
#if TENJIN_ANALYTICS
            var settings = TenjinSettingsProvider.GetSettings();
            _instance = Tenjin.getInstance(settings.ApiKey);
#if UNITY_ANDROID
            _instance.SetAppStoreType(AppStoreType.googleplay);
#endif
            _instance.Connect();
#endif
        }
    }
}