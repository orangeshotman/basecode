using System.Collections.Generic;

namespace OrangeShotStudio.Analytics
{
    public class UnityAnalytics : IAnalyticsService
    {
        public void SendProgressEvent(ProgressEventName eventName, string levelName, int? score = null)
        {
            switch (eventName)
            {
                case ProgressEventName.Start:
                    SendCustomEvent("Start", new Dictionary<string, object>(){{"LevelName", levelName}});
                    break;
                case ProgressEventName.Complete:
                    SendCustomEvent("Complete", new Dictionary<string, object>(){{"LevelName", levelName}});
                    break;
                case ProgressEventName.Fail:
                    SendCustomEvent("Fail", new Dictionary<string, object>(){{"LevelName", levelName}});
                    break;
            }
        }

        public void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
            if (parameters.Count > 0)
                UnityEngine.Analytics.Analytics.CustomEvent(eventName, parameters);
            else
                UnityEngine.Analytics.Analytics.CustomEvent(eventName);
            UnityEngine.Analytics.Analytics.FlushEvents();
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            
        }

        public void OnApplicationExit()
        {
            
        }

        public void ShowDebug()
        {
            
        }
    }
}