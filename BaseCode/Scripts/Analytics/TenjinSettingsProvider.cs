using UnityEngine;

namespace OrangeShotStudio.Analytics
{
    public static class TenjinSettingsProvider
    {
        public static TenjinSettings GetSettings()
        {
            return Resources.Load<TenjinSettings>("TenjinSettings");
        }
    }
}