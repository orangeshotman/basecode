﻿using UnityEngine;

namespace OrangeShotStudio.CarDrift.View.UI
{
    public class MaterialOffsetAnimation : MonoBehaviour
    {
        public Renderer Renderer;
        public float Speed;
        public int MaterialIndex;
        private Material _material;
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");

        public void Awake()
        {
            _material = Renderer.materials[MaterialIndex];
        }

        public void Update()
        {
            _material.SetTextureOffset(MainTex, new Vector2(Time.time * Speed % 1f, 0));
        }
    }
}