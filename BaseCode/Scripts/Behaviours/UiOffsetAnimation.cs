﻿using UnityEngine;
using UnityEngine.UI;

namespace OrangeShotStudio.CarDrift.View.UI
{
    public class UiOffsetAnimation : MonoBehaviour
    {
        public RawImage Renderer;
        public float Speed;
        public Vector2 Tiling = Vector2.one;
        public void Update()
        {
            Renderer.uvRect = new Rect(new Vector2(Time.time*Speed,0), Tiling);
        }
    }
}