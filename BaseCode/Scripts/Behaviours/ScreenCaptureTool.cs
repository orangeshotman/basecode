﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCaptureTool : MonoBehaviour {

	// Use this for initialization
    private int index;
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.P))
	    {
            Debug.Log("Print");
            ScreenCapture.CaptureScreenshot("TestScreenCapture" + index++ + ".png", 1);
	    }
	}
}
