﻿using UnityEngine;

namespace OrangeShotStudio.Behaviours
{
    public class SimpleLine : MonoBehaviour, ILevelObjectOperations
    {
        public RoadDefinition _RoadDefinition;
        protected SpriteRenderer _spriteRenderer;
        
        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (_spriteRenderer == null)
                    _spriteRenderer = GetComponent<SpriteRenderer>();
                return _spriteRenderer;
            }
        }

        public RoadDefinition RoadDefinition { get { return _RoadDefinition; } }
        public Vector2 Position { get { return SpriteRenderer.transform.position; } }

        public virtual Vector2 Down
        {
            get { return transform.position - Vector3.up*(SpriteRenderer.size.y/2); }
        }

        public virtual Vector2 Up
        {
            get { return transform.position + Vector3.up*(SpriteRenderer.size.y/2); }
        }

        public Vector2 Size { get { return SpriteRenderer.size; } }


        public virtual bool Dangerous
        {
            get { return false; }
        }
        

        public virtual void SetPositions(Vector2 startPos, Vector2 endPos)
        {
            transform.position = startPos + (endPos - startPos)/2;
            SpriteRenderer.size = new Vector2(SpriteRenderer.size.x, (startPos - endPos).magnitude);
        }

        public void Dispose()
        {
            Destroy(gameObject);
        }
    }
}