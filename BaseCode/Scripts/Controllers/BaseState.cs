namespace OrangeShotStudio.Scripts.Controllers
{
    public abstract class BaseState<TInternalContext, TExternalContext, TDerivativeBaseState> 
    {
        public TDerivativeBaseState NextState { get; set; }
        private bool _initialized;
        public void Update(TExternalContext externalContext, TInternalContext internalContext)
        {
            if (!_initialized)
            {
                InternalOnEnter(externalContext, internalContext);
                _initialized = true;
            }
            InternalUpdate(externalContext, internalContext);
        }

        protected abstract void InternalOnEnter(TExternalContext externalContext, TInternalContext internalContext);
        protected abstract void InternalUpdate(TExternalContext externalContext, TInternalContext internalContext);

    }
}