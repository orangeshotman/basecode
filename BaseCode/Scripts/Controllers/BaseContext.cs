using UnityEngine;

namespace OrangeShotStudio.Scripts.Controllers
{
    public abstract class BaseContext<TInternalContext, TExternalContext, TDerivativeBaseState> where TDerivativeBaseState : BaseState<TInternalContext, TExternalContext, TDerivativeBaseState>
    {
        protected TDerivativeBaseState CurrentState;
        protected void Update(TExternalContext externalContext, TInternalContext context)
        {
            CurrentState.Update(externalContext, context);
            if (CurrentState.NextState != null)
            {
                Debug.Log($"[{typeof(TInternalContext)}] From {CurrentState} to {CurrentState.NextState}");
                CurrentState = CurrentState.NextState;
            }
        }
    }
}