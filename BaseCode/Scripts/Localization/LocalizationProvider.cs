﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeShotStudio.Localization
{
    public class LocalizationProvider : ILocalizationProvider
    {
        public static LocalizationProvider Instance;
        private Dictionary<string, string> _dictionary = new Dictionary<string, string>();
        private Dictionary<SystemLanguage, SystemLanguage> _castDic = new Dictionary<SystemLanguage, SystemLanguage>() { {SystemLanguage.Russian, SystemLanguage.English} }; 

        public LocalizationProvider()
        {
            var csv = Resources.Load<TextAsset>("Localization");
            string[] headers = csv.text.Substring(0,csv.text.IndexOf(Environment.NewLine, StringComparison.Ordinal)).Split(',');
            var currentLanguage = UnityEngine.Application.systemLanguage;
            if (_castDic.ContainsKey(currentLanguage))
                currentLanguage = _castDic[currentLanguage];
            int column = 1;
            for (int index = 1; index < headers.Length; index++)
            {
                var header = headers[index];
                if (header == currentLanguage.ToString())
                {
                    column = index;
                    Debug.Log("Current language is " + header);
                    break;
                }
            }
            string[] rows = csv.text.Split('\n');
            foreach (var row in rows)
            {
                string[] values = row.Split(',');
                _dictionary[values[0]] = values[column].Trim();
            }
            Instance = this;
        }

        public string GetLocalized(string key)
        {
            if (_dictionary.ContainsKey(key))
            {
                return _dictionary[key];
            }
            string result = "Key " + key + " not found";
            Debug.LogError(result);
            return result;
        }

    }
}