namespace OrangeShotStudio.Localization
{
    public static class LocalizeExtension
    {
        public static string Localize(this string text)
        {
            return LocalizationProvider.Instance.GetLocalized(text);
        }

        public static string SetParameters(this string text, object val)
        {
            return text.Replace("@n", val.ToString());
        }
    }
}