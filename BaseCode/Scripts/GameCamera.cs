﻿using OrangeShotStudio.Provider;
using UnityEngine;

namespace OrangeShotStudio.Behaviours
{
    public class GameCamera : ICamera
    {
        private Camera _camera;
        private AudioListener _listener;
        public GameCamera(IPrefabProvider prefabProvider)
        {
            _camera = Object.Instantiate(prefabProvider.GetPrefab("Camera")).GetComponent<Camera>();
            _listener = _camera.GetComponentInChildren<AudioListener>();
        }

        public Camera Camera { get { return _camera; } }

        public Vector2 Position
        {
            set { _camera.transform.position = new Vector3(value.x, value.y, _camera.transform.position.z); }
            get { return _camera.transform.position; }
        }

        public void SetAudio(bool val)
        {
            AudioListener.volume = val ? 1 : 0;
        }

        public void Dispose()
        {
            Object.Destroy(_camera.gameObject);
        }
    }
}