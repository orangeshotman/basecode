using System;
using System.Diagnostics;
using OrangeShotStudio.Structuries;
using UnityEngine;

namespace OrangeShotStudio.Application
{
    public class ApplicationUpdater : MonoBehaviour
    {
        private IGameApplication _app;
        private Stopwatch _stopwatch = Stopwatch.StartNew();
        private float _lastTimeUpdate;

        public void Initialize(IGameApplication app)
        {
            _app = app;
        }

        public void FixedUpdate()
        {
            if (_app != null)
            {
                var currentTime = _stopwatch.ElapsedMilliseconds / 1000f;
                Time.fixedDeltaTime = 0.02F * Time.timeScale;
                _app.FixedUpdate(new TimeData {TimeSinceStartUp = currentTime, DeltaTime = 0.02f});
            }
        }

        public void Update()
        {
            var currentTime = _stopwatch.ElapsedMilliseconds / 1000f;
            _app?.Update(new TimeData { TimeSinceStartUp = currentTime, DeltaTime = currentTime - _lastTimeUpdate});
            _lastTimeUpdate = currentTime;
        }
        
        public void LateUpdate()
        {
            var currentTime = _stopwatch.ElapsedMilliseconds / 1000f;
            _app?.LateUpdate(new TimeData { TimeSinceStartUp = currentTime, DeltaTime = currentTime - _lastTimeUpdate});
            _lastTimeUpdate = currentTime;
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            _app.OnApplicationPause(pauseStatus);
        }

        private void OnApplicationQuit()
        {
            _app.OnQuit();
        }
    }
}