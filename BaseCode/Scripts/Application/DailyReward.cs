﻿using System;
using System.Collections.Generic;
using System.Globalization;
using OrangeShotStudio.Analytics;
using OrangeShotStudio.Data;
using UnityEngine;

namespace OrangeShotStudio.CarDrift.Application
{
    public class DailyReward : IDailyReward
    {
        private readonly IAnalyticsService _analyticsService;
        private readonly INotificationManager _notificationManager;
        private readonly PrefsPropertyInt _lastRewardIndex;
        private readonly PrefsPropertyList<int> _rewardsList;
        private readonly PrefsPropertyString _lastRewardTime;


        public DailyReward(IAnalyticsService analyticsService, INotificationManager notificationManager)
        {
            _analyticsService = analyticsService;
            _notificationManager = notificationManager;
            _lastRewardIndex = new PrefsPropertyInt("LastRewardIndexKey", -1);
            _rewardsList = new PrefsPropertyList<int>("DailyRewardListKey", new List<int>() { 500, 1000, 3000, 5000, 8000 });
            _lastRewardTime = new PrefsPropertyString("LastRewardTimeKey", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            _notificationManager.CancelAll();
            var left = LeftToReward;
            if (left.TotalSeconds > 0)
                _notificationManager.SendWithAppIcon(left, "Car Drift", "Daily reward is ready!", Color.white);

        }

        public TimeSpan LeftToReward
        {
            get
            {
                var result = DateTime.Parse(_lastRewardTime.Value).AddDays(1) - DateTime.UtcNow;
                return result;
            }
        }

        public List<int> RewardList
        {
            get { return _rewardsList.List; } 
        }

        public int LastRewardIndex
        {
            get { return _lastRewardIndex.Value; }
            private set { _lastRewardIndex.Value = value; }
        }

        public int GetReward()
        {
            if (LeftToReward.TotalSeconds < 0)
            {
                int currentIndex = _lastRewardIndex.Value;
                currentIndex = (currentIndex + 1)%RewardList.Count;
                _analyticsService.SendCustomEvent("DailyReward", new Dictionary<string, object>() { { "afterTimeInHours", LeftToReward.Negate().Hours }, { "reward", RewardList[currentIndex] } });
                LastRewardIndex = currentIndex;
                _lastRewardTime.Value = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);
                var left = LeftToReward;
                if (left.TotalSeconds > 0)
                    _notificationManager.SendWithAppIcon(left, "Car Drift", "Daily reward is ready!", Color.white);
                return RewardList[currentIndex];
            }
            return 0;
        }
    }
}
