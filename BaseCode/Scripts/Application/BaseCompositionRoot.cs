﻿using OrangeShotStudio.Application;
using OrangeShotStudio.Behaviours;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;
using Zenject;

namespace OrangeShotStudio.DB.Application
{
    public abstract class BaseCompositionRoot : ICompositionRoot
    {
        protected DiContainer Container;

        protected BaseCompositionRoot()
        {
            Container = new DiContainer();
        }

        protected abstract void Initialize();

        protected abstract void Bind();
        
        public void Launch()
        {
            Container.Bind<InterfaceView>().ToSelf().AsSingle();
            Container.Bind<ICamera>().To<GameCamera>().AsSingle();
            Container.Bind<IPrefabProvider>().To<PrefabProvider>().AsSingle();
            Bind();
            Initialize();
            
        }
    }
}