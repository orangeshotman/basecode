﻿using UnityEngine;
namespace OrangeShotStudio.Application
{
    public abstract class BaseLauncher : MonoBehaviour
    {
        protected abstract ICompositionRoot Cr { get; }

        public virtual void Awake()
        {
            UnityEngine.Application.targetFrameRate = 60;
            Cr.Launch();
        }
    }



}