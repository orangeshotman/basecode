﻿using OrangeShotStudio.Analytics;
using OrangeShotStudio.Structuries;
using OrangeShotStudio.View;
using UnityEngine;

namespace OrangeShotStudio.Application
{
    public abstract class BaseGameController : IGameApplication
    {
        private readonly IAnalyticsService _analyticsService;
        protected IGameState GameState;

        protected BaseGameController(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
            GameState = new PlugGameState();
            var go = new GameObject("Updater");
            Object.DontDestroyOnLoad(go);
            go.AddComponent<ApplicationUpdater>().Initialize(this);
        }

        void IGameApplication.FixedUpdate(TimeData timeData)
        {
            GameState.FixedUpdate(timeData);
            InternalFixedUpdate(timeData);
            CheckNewState();
        }

        void IGameApplication.Update(TimeData timeData)
        {
            GameState.Update(timeData);
            InternalUpdate(timeData);
            CheckNewState();
        }

        void IGameApplication.LateUpdate(TimeData timeData)
        {
            GameState.LateUpdate(timeData);
            InternalLateUpdate(timeData);
            CheckNewState();
        }

        protected virtual void InternalFixedUpdate(TimeData timeData)
        {
        }

        protected virtual void InternalUpdate(TimeData timeData)
        {
        }

        protected virtual void InternalLateUpdate(TimeData timeData)
        {
        }

        void IGameApplication.OnApplicationPause(bool pauseStatus)
        {
            GameState.OnApplicationPause(pauseStatus);
            _analyticsService.OnApplicationPause(pauseStatus);
        }

        void IGameApplication.OnQuit()
        {
            GameState.OnQuit();
            _analyticsService.OnApplicationExit();
        }

        private void CheckNewState()
        {
            if (GameState.NextState != null && GameState.NextState != GameState)
            {
                PreStateChange();
                GameState = GameState.NextState;
                GameState.Initialize();
            }
        }

        protected abstract void PreStateChange();
    }
}