﻿using System;
using System.Collections.Generic;

namespace OrangeShotStudio.CarDrift.Application
{
    public interface IDailyReward
    {
        TimeSpan LeftToReward { get; }
        List<int> RewardList { get; } 
        int LastRewardIndex { get; }
        int GetReward();
    }
}