﻿using System;
using UnityEngine;

namespace OrangeShotStudio.CarDrift.Application
{
    public interface INotificationManager
    {
        void CancelAll();
        int SendWithAppIcon(TimeSpan delay, string title, string message, Color smallIconColor);
    }
}