using OrangeShotStudio.Structuries;
using UnityEngine;

namespace OrangeShotStudio.Application
{
    public class PlugGameState : IGameState
    {
        public IGameState NextState { get; }

        public void OnApplicationPause(bool pauseStatus)
        {
            
        }

        public void OnQuit()
        {
        }

        public void Initialize()
        {
            
        }

        public void FixedUpdate(TimeData timeData)
        {
        }

        public void Update(TimeData timeData)
        {
        }

        public void LateUpdate(TimeData timeData)
        {
            
        }

        public void Input(string input)
        {
        }

        public void Input(KeyCode input)
        {
            
        }
    }
}