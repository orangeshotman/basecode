using OrangeShotStudio.Behaviours;
using OrangeShotStudio.Provider;
using OrangeShotStudio.View;

namespace OrangeShotStudio.DB.Application
{
    public abstract class BaseGameStateFactory
    {
        protected InterfaceView InterfaceView;
        protected IPrefabProvider PrefabProvider;
        protected ICamera Camera;

        public BaseGameStateFactory(IPrefabProvider prefabProvider, InterfaceView interfaceView)
        {
            PrefabProvider = prefabProvider;
            InterfaceView = interfaceView;
        }
    }
}