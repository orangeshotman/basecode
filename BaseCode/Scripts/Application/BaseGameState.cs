﻿using OrangeShotStudio.Structuries;
using UnityEngine;

namespace OrangeShotStudio.Application
{
    public abstract class BaseGameState : IGameState
    {
        public IGameState NextState { get; protected set; }

        public virtual void Initialize()
        {
        }

        public virtual void FixedUpdate(TimeData timeData)
        {
        }

        public virtual void Update(TimeData timeData)
        {
        }

        public virtual void LateUpdate(TimeData timeData)
        {
        }

        public virtual void Input(string input)
        {
        }

        public virtual void Input(KeyCode input)
        {
        }

        public virtual void OnApplicationPause(bool pauseStatus)
        {
        }

        public virtual void OnQuit()
        {
        }
    }
}