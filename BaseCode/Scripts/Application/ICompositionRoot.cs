﻿namespace OrangeShotStudio.Application
{
    public interface ICompositionRoot
    {
        void Launch();
    }
}