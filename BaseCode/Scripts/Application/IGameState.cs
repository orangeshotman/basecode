﻿using OrangeShotStudio.Structuries;
using UnityEngine;

namespace OrangeShotStudio.Application
{
    public interface IGameState
    {
        IGameState NextState { get; }
        void Initialize();
        void FixedUpdate(TimeData timeData);
        void Update(TimeData timeData);
        void LateUpdate(TimeData timeData);
        void OnApplicationPause(bool pauseStatus);
        void OnQuit();
    }
}