using UnityEngine;
using UnityEngine.SceneManagement;

namespace OrangeShotStudio.Application
{
    public class SceneLoader
    {
        public float Progress => _asyncOperation.progress;
        public bool IsDone => _asyncOperation.isDone;
        private readonly ISceneInitializer _sceneInitializer;
        private bool _initialized;
        private AsyncOperation _asyncOperation;

        public SceneLoader(string levelName, ISceneInitializer sceneInitializer)
        {
            _sceneInitializer = sceneInitializer;
            _asyncOperation = SceneManager.LoadSceneAsync(levelName);
        }

        public void Update()
        {
            if (_asyncOperation.isDone && !_initialized)
            {
                _sceneInitializer.InitializeScene();
                _initialized = true;
            }
        }
    }
}