﻿using System;
using UnityEngine;
#if NOTIFICATIONS
using Assets.SimpleAndroidNotifications;
#endif

namespace OrangeShotStudio.CarDrift.Application
{
    public class NotificationManager : INotificationManager
    {
        public void CancelAll()
        {
#if NOTIFICATIONS
            Assets.SimpleAndroidNotifications.NotificationManager.CancelAll();
#endif
        }

        public int SendWithAppIcon(TimeSpan delay, string title, string message, Color smallIconColor)
        {
#if NOTIFICATIONS
            return Assets.SimpleAndroidNotifications.NotificationManager.SendWithAppIcon(delay, title, message, smallIconColor, NotificationIcon.Event);
#else
            return 0;
#endif

        }
    }
}