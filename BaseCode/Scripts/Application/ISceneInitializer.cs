namespace OrangeShotStudio.Application
{
    public interface ISceneInitializer
    {
        void InitializeScene();
    }
}