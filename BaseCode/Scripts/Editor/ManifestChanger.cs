using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace OrangeShotStudio.Tools
{
    public static class ManifestChanger
    {
        public const string GameAnalyticsPackage =
            "\"com.gameanalytics.sdk\": \"https://github.com/GameAnalytics/GA-SDK-UNITY.git\",\n    "; 
        public const string NSubstitutePackage =
            "\"net.tnrd.nsubstitute\": \"https://github.com/Thundernerd/Unity3D-NSubstitute.git\",\n    ";
        [MenuItem("OrangeShot/Add game analytics")]
        public static void AddGameAnalytics()
        {
            AddPackage(GameAnalyticsPackage);
        }
        
        [MenuItem("OrangeShot/Add NSubstitute")]
        public static void AddNSubstitute()
        {
            AddPackage(NSubstitutePackage);
        }

        public static void AddPackage(string package)
        {
            var path = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "Packages", "manifest.json"));
            string text = String.Empty;
            using (var textStream = File.OpenText(path))
            {
                text = textStream.ReadToEnd();
                
            }

            var find = "\"dependencies\": {";
            var index = text.IndexOf(find, StringComparison.Ordinal)+find.Length+5;
            text = text.Insert(index, package);
            File.WriteAllText(path, text);
            Debug.Log("manifest.json had been updated");
            AssetDatabase.Refresh();
        }
    }
}