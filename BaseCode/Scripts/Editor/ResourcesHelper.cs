﻿using System.Linq;
using OrangeShotStudio.Data;
using UnityEditor;
using UnityEngine;

namespace OrangeShotStudio.Tools
{
    public class ResourcesHelper
    {
        [MenuItem("Assets/Add to prefabs")]
        public static void AddToPrefabs()
        {
            var prefab = (GameObject) Selection.activeObject;
            var db = Resources.Load<PrefabDb>("PrefabDb");
            var list = db.PrefabData.ToList();
            list.Add(new PrefabData()
            {
                Id = prefab.name,
                Prefab = prefab
            });
            db.PrefabData = list;
            EditorUtility.SetDirty(db);
            Debug.Log(prefab.name + " has been added to DB");
        }

        [MenuItem("Assets/Complex prefabs provider/Add to server")]
        public static void AddToServerPrefabs()
        {
            var prefab = (GameObject) Selection.activeObject;
            var db = Resources.Load<ComplexPrefabDb>("ComplexPrefabDb");
            var list = db.PrefabData.ToList();
            var data = list.FirstOrDefault(x => x.Id == prefab.name);
            if (data == null)
            {
                data = new ComplexPrefabData()
                {
                    Id = prefab.name,
                    Prefabs = new GameObject[3]
                };
                list.Add(data);
            }

            data.Prefabs[0] = prefab;
            db.PrefabData = list;
            EditorUtility.SetDirty(db);
            Debug.Log(prefab.name + " has been added to DB");
        }

        [MenuItem("Assets/Complex prefabs provider/Add to client")]
        public static void AddToClientPrefabs()
        {
            var prefab = (GameObject) Selection.activeObject;
            var db = Resources.Load<ComplexPrefabDb>("ComplexPrefabDb");
            var list = db.PrefabData.ToList();
            var data = list.FirstOrDefault(x => x.Id == prefab.name);
            if (data == null)
            {
                data = new ComplexPrefabData()
                {
                    Id = prefab.name,
                    Prefabs = new GameObject[3]
                };
                list.Add(data);
            }

            data.Prefabs[1] = prefab;
            db.PrefabData = list;
            EditorUtility.SetDirty(db);
            Debug.Log(prefab.name + " has been added to DB");
        }
    }
}