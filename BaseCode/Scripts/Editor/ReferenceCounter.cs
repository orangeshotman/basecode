﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace OrangeShotStudio.Tools
{
    public class ReferenceCounter : EditorWindow
    {
        public Object CheckingAsset;
        private List<Object> _results = new List<Object>();
        private Vector2 _scroll;
        [MenuItem("OrangeShot/Reference counter")]
        public static void Init()
        {
            ReferenceCounter window = (ReferenceCounter) GetWindow(typeof (ReferenceCounter));
            window.Show();
        }
        [MenuItem("Assets/Find all references")]
        public static void InitAndFind()
        {
            ReferenceCounter window = (ReferenceCounter)GetWindow(typeof(ReferenceCounter));
            window.CheckingAsset = Selection.activeObject;
            window.Show();
            window.FindReferences();
        }
        void OnGUI()
        {
            var so = new SerializedObject(this);
            EditorGUILayout.PropertyField(so.FindProperty("CheckingAsset"));
            if(GUILayout.Button("Find references"))
            {
                FindReferences();   
            }
            _scroll = EditorGUILayout.BeginScrollView(_scroll);
            foreach(var result in _results)
            {
                EditorGUILayout.ObjectField(result, result.GetType());
            }
            EditorGUILayout.EndScrollView();
            so.ApplyModifiedProperties();
        }
        public void FindReferences()
        {
            var assets = AssetDatabase.FindAssets("t:Object");
            _results = new List<Object>();
            foreach (var asset in assets)
            {
                var obj = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GUIDToAssetPath(asset));
                if (obj == CheckingAsset)
                    continue;
                var results = EditorUtility.CollectDependencies(new[] { obj });
                for (var index = 0; index < results.Length; index++)
                {
                    var result = results[index];
                    if (result == CheckingAsset)
                    {
                        _results.Add(obj);
                        break;
                    }
                }
            }
        }
    }
}