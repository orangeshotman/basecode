﻿using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using OrangeShotStudio.Structuries;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    [TestFixture]
    public class ListObserver
    {
        [Test]
        public void ProcessableListObserverAddTest()
        {
            var observer = Substitute.For<IListObserver<int>>();
            ProcessableListObserver<int> processableListObserver = new ProcessableListObserver<int>();
            processableListObserver.Add(1);
            processableListObserver.ProcessChanges(observer);
            observer.Received().Add(Arg.Is<int>(x => x == 1));
        }

        [Test]
        public void ProcessableListObserverRemoveTest()
        {
            var observer = Substitute.For<IListObserver<int>>();
            ProcessableListObserver<int> processableListObserver = new ProcessableListObserver<int>();
            processableListObserver.Remove(1);
            processableListObserver.ProcessChanges(observer);
            observer.Received().Remove(Arg.Is<int>(x => x == 1));
        }
        
        [Test]
        public void ProcessableListObserverMultipleAddTest()
        {
            var observer = Substitute.For<IListObserver<int>>();
            ProcessableListObserver<int> processableListObserver = new ProcessableListObserver<int>();
            processableListObserver.Add(1);
            processableListObserver.Add(2);
            processableListObserver.Add(3);
            processableListObserver.ProcessChanges(observer);
            observer.Received(3).Add(Arg.Any<int>());
        }
        
        [Test]
        public void ProcessableListObserverMultipleRemoveTest()
        {
            var observer = Substitute.For<IListObserver<int>>();
            ProcessableListObserver<int> processableListObserver = new ProcessableListObserver<int>();
            processableListObserver.Remove(1);
            processableListObserver.Remove(2);
            processableListObserver.Remove(3);
            processableListObserver.ProcessChanges(observer);
            observer.Received(3).Remove(Arg.Any<int>());
        }
    }
}