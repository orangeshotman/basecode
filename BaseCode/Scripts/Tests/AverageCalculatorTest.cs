using Assets.Scripts.Model;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class AverageCalculatorTest
    {
        [Test]
        public void NotFullAverageTest()
        {
            AverageCalculator averageCalculator = new AverageCalculator(10);
            averageCalculator.Add(5);
            Assert.AreEqual(5, averageCalculator.Average);
        }
        
        [Test]
        public void TwoElementsAverageTest()
        {
            AverageCalculator averageCalculator = new AverageCalculator(10);
            averageCalculator.Add(2);
            averageCalculator.Add(4);
            Assert.AreEqual(3, averageCalculator.Average);
        }
        
        [Test]
        public void FullAverageTest()
        {
            AverageCalculator averageCalculator = new AverageCalculator(10);
            for (int i = 0; i < 5; i++)
            {
                averageCalculator.Add(2);
                averageCalculator.Add(4);
            }
            Assert.AreEqual(3, averageCalculator.Average);
        }
        
        [Test]
        public void OverFullAverageTest()
        {
            AverageCalculator averageCalculator = new AverageCalculator(10);
            for (int i = 0; i < 10; i++)
            {
                averageCalculator.Add(2);
                averageCalculator.Add(4);
            }
            Assert.AreEqual(3, averageCalculator.Average);
        }
        
        
    }
}