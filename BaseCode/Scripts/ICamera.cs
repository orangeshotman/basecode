﻿using System;
using UnityEngine;

namespace OrangeShotStudio.Behaviours
{
    public interface ICamera : IDisposable
    {
        Camera Camera { get; }
        Vector2 Position { get; set; }
        void SetAudio(bool val);
    }
}