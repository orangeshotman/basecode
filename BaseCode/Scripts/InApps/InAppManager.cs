﻿//using System;
//using System.Collections.Generic;
//using OrangeShotStudio.Data;
//using OrangeShotStudio.Statistic;
//using UnityEngine;
//using UnityEngine.Purchasing;
//using UnityEngine.Purchasing.Security;


//namespace OrangeShotStudio.InnApp
//{
//    public class InAppManager : IStoreListener, IInAppManager
//    {
//        private readonly IAnalyticsService _analyticsService;
//        private PrefsPropertyList<string> _purchasedProducts;
//        private IStoreController _controller;
//        private IExtensionProvider _extensions;

//        public event Action OnPurchaseUpdate = () => { };

//        public List<string> PurchasedProducts
//        {
//            get { return _purchasedProducts.List; }
//        }

//        public InAppManager(IAnalyticsService analyticsService)
//        {
//            _analyticsService = analyticsService;
//            Initialize();
//        }

//        public void Initialize()
//        {
//            _purchasedProducts = new PrefsPropertyList<string>("PurchasedProductsKey", new List<string>());
//            var config = Resources.Load<TextAsset>("InAppConfig");
//            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
//            var raws = config.text.Split('\n');
//            for (int index = 1; index < raws.Length; index++)
//            {
//                var raw = raws[index].Trim();
//                var values = raw.Split(',');
//                if (values.Length > 2)
//                {
//                    builder.AddProduct(values[0], ProductType.NonConsumable);
//                }
//            }
//            UnityPurchasing.Initialize(this, builder);
//        }


//        public void Buy(string id)
//        {
//            if (_controller != null)
//            {
//                _analyticsService.SendCustomEvent("InitiatePurchase", new Dictionary<string, object>() { {id, "try"} });
//                _controller.InitiatePurchase(id);
//            }
//            else
//            {
//                _analyticsService.SendCustomEvent("InitiatePurchase", new Dictionary<string, object>() { {id, "fail"} });
//                Debug.LogError("In-Apps is not initialized");
//            }
//        }

//        public Product GetProductInfo(string id)
//        {
//            if (_controller != null)
//            {
//                return _controller.products.WithID(id);
//            }
//            else
//            {
//                Debug.LogError("In-Apps is not initialized");
//                return null;
//            }
//        }

//        public void OnInitializeFailed(InitializationFailureReason error)
//        {
//            _analyticsService.SendCustomEvent("InAppOnInitializeFailed", new Dictionary<string, object>() {});
//            Debug.LogError(error);
//        }

//        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
//        {

//            bool validPurchase = true; // Presume valid for platforms with no R.V.
//            // Unity IAP's validation logic is only included on these platforms.
//#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
//            //// Prepare the validator with the secrets we prepared in the Editor
//            //// obfuscation window.
//            //var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
//            //    AppleTangle.Data(), UnityEngine.Application.identifier);

//            //try
//            //{
//            //    // On Google Play, result has a single product ID.
//            //    // On Apple stores, receipts contain multiple products.
//            //    var result = validator.Validate(e.purchasedProduct.receipt);
//            //    // For informational purposes, we list the receipt(s)
//            //    Debug.Log("Receipt is valid. Contents:");
//            //    foreach (IPurchaseReceipt productReceipt in result)
//            //    {
//            //        Debug.Log(productReceipt.productID);
//            //        Debug.Log(productReceipt.purchaseDate);
//            //        Debug.Log(productReceipt.transactionID);
//            //    }
//            //}
//            //catch (IAPSecurityException)
//            //{
//            //    Debug.Log("Invalid receipt, not unlocking content");
//            //    validPurchase = false;
//            //}
//#endif

//            if (validPurchase)
//            {
//                var list = _purchasedProducts.List;
//                list.Add(e.purchasedProduct.definition.id);
//                _purchasedProducts.List = list;
//                OnPurchaseUpdate();
//                _analyticsService.SendCustomEvent("Purchase", new Dictionary<string, object>() { {e.purchasedProduct.definition.id, e.purchasedProduct.metadata.localizedPriceString+e.purchasedProduct.metadata.isoCurrencyCode} });
//            }
            
//            return PurchaseProcessingResult.Complete;
//        }

//        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
//        {
//            _analyticsService.SendCustomEvent("OnPurchaseFailed", new Dictionary<string, object>() {});
//            Debug.LogError(i.definition.id + " " + p);
//        }

//        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
//        {
//            _analyticsService.SendCustomEvent("InAppOnInitialized", new Dictionary<string, object>() {});
//            Debug.Log("InAppManager initialized");
//            _extensions = extensions;
//            _controller = controller;
//        }
//    }
//}

