namespace OrangeShotStudio.Structuries
{
    public struct TimeData
    {
        public float TimeSinceStartUp;
        public float DeltaTime;
    }
}