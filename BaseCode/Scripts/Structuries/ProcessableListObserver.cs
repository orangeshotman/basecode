using System.Collections.Generic;

namespace OrangeShotStudio.Structuries
{
    public class ProcessableListObserver<T> : IListObserver<T>
    {
        private Queue<ListChanging<T>> _changes = new Queue<ListChanging<T>>();

        public void Add(T item)
        {
            _changes.Enqueue(new ListChanging<T>() {Add = true, Item = item});
        }

        public void Remove(T item)
        {
            _changes.Enqueue(new ListChanging<T>() {Add = false, Item = item});
        }

        public void ProcessChanges(IListObserver<T> observer)
        {
            while (_changes.Count > 0)
            {
                var item = _changes.Dequeue();
                if(item.Add)
                    observer.Add(item.Item);
                else
                    observer.Remove(item.Item);
            }
        }

        private struct ListChanging<T>
        {
            public bool Add;
            public T Item;
        }
    }
}