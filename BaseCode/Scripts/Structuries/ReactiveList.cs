using System;
using System.Collections.Generic;

namespace OrangeShotStudio.Structuries
{
    public class ReactiveList<T> : List<T>, IReactiveList<T>
    {
        public event Action<T> OnAdd = obj => { };  
        public event Action<T> OnRemove = obj => { };  
        
        public new void Add(T element)
        {
            base.Add(element);
            OnAdd(element);
        } 
        
        public new void RemoveAt(int index)
        {
            var item = this[index];
            base.RemoveAt(index);
            OnRemove(item);
        }
        
        public new void Remove(T element)
        {
            base.Remove(element);
            OnRemove(element);
        }
        
        public new void Clear()
        {
            for (var index = 0; index < this.Count; index++)
            {
                var element = this[index];
                OnRemove(element);
            }

            base.Clear();
        } 
    }
}