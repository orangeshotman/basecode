namespace Assets.Scripts.Model
{
    public interface IDefinition
    {
        string Id { get; }
    }
}