﻿using System;

namespace OrangeShotStudio.Behaviours
{
    [Serializable]
    public class RoadDefinition
    {
        public string Id;
        public bool TwoWay;
        public bool WithoutLeftSide;
        public bool WithoutRightSide;
    }
}