using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeShotStudio.Structuries
{
    public sealed class Pool<T>
    {
        private readonly Func<T> _factory;
        private readonly Action<T> _disposeMethod;
        private readonly Stack<T> _free;
        private int _totalSize;

        public Pool(Func<T> factory, Action<T> disposeMethod, int size = 10)
        {
            _factory = factory;
            _disposeMethod = disposeMethod;
            _free = new Stack<T>(size);
            for (int i = 0; i < size; i++)
            {
                _free.Push(_factory());
            }

            _totalSize = size;
        }

        public PoolObject<T> Get()
        {
            if (_free.Count == 0)
            {
                _totalSize++;
                Debug.LogError($"Check pool leaking. Current size {_totalSize}");
                return PoolObject<T>.Create(this, _factory(), _disposeMethod);
            }

            return PoolObject<T>.Create(this, _free.Pop(), _disposeMethod);
        }

        public void Release(T p)
        {
            if (p == null)
            {
                throw new ArgumentNullException();
            }

            _free.Push(p);
        }
    }
}