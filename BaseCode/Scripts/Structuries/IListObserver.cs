namespace OrangeShotStudio.Structuries
{
    public interface IListObserver<T>
    {
        void Add(T item);
        void Remove(T item);
    }
}