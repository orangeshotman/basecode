﻿using System;
using Assets.Scripts.Model;
using UnityEngine;

namespace OrangeShotStudio.Behaviours
{
    public interface ILevelObject : IDisposable
    {
        RoadDefinition RoadDefinition { get; }
        Vector2 Position { get; }
        Vector2 Down { get; }
        Vector2 Up { get; }
        Vector2 Size { get; }
    }

    public interface ILevelObjectOperations : ILevelObject
    {
        void SetPositions(Vector2 startPos, Vector2 endPos);
    }
}