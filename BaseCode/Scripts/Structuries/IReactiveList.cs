using System;
using System.Collections.Generic;

namespace OrangeShotStudio.Structuries
{
    public interface IReactiveList<T> : IReadOnlyList<T>
    {
        event Action<T> OnAdd;
        event Action<T> OnRemove;
    }
}