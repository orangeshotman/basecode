using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeShotStudio.Structuries
{
    //TODO похоже что структура не подойдет... либо надо чёт придумать
    public class PoolObject<T> : IEquatable<PoolObject<T>>
    {
        private static Stack<PoolObject<T>> _poolObjects = new Stack<PoolObject<T>>();
        public T Value;
        private readonly Action<T> _disposeMethod;
        private Pool<T> _pool;
        private bool _disposed;

        public static PoolObject<T> Create(Pool<T> pool, T value, Action<T> disposeMethod)
        {
            if (_poolObjects.Count > 0)
            {
                var result = _poolObjects.Pop();
                result._disposed = false;
                result.Value = value;
                result._pool = pool;
                return result;
            }
            return new PoolObject<T>(pool, value, disposeMethod);
        }
        
        private PoolObject(Pool<T> pool, T value, Action<T> disposeMethod)
        {
            _pool = pool;
            Value = value;
            _disposeMethod = disposeMethod;
            _disposed = false;
        }

        public void Dispose()
        {
            if (_disposed)
                return;
            _pool.Release(Value);
            _disposeMethod(Value);
            Value = default;
            _disposed = true;
            _poolObjects.Push(this);
        }

        public bool Equals(PoolObject<T> other)
        {
            return EqualityComparer<T>.Default.Equals(Value, other.Value) && Equals(_pool, other._pool) &&
                   _disposed == other._disposed;
        }

        public override bool Equals(object obj)
        {
            return obj is PoolObject<T> other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EqualityComparer<T>.Default.GetHashCode(Value);
                hashCode = (hashCode * 397) ^ (_pool != null ? _pool.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _disposed.GetHashCode();
                return hashCode;
            }
        }
    }
}