using DG.Tweening;
using TMPro;

namespace OrangeShotStudio.Scripts.Tools
{
    public static class TweenExtensions
    {
        public  static void AnimateText(this TextMeshProUGUI text, int start, int finish)
        {
            DOTween.To(() => start, x =>
            {
                text.text = x.ToString("0.##");
                start = x;
            }, finish, 0.3f);
        }
    }
}