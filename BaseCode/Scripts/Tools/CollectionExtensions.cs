using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OrangeShotStudio.Scripts.Tools
{
    public static class CollectionExtensions
    {
        public static T RandomElement<T>(this IEnumerable<T> sequence)
        {
            T current = default(T);
            int n = 0;
            foreach (var element in sequence)
            {
                n++;
                if (Random.Range(0, n) == 0)
                    current = element;
            }

            return current;
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> sequence)
        {
            return sequence.OrderBy(_ => Random.Range(0, 100));
        }

        public static T RandomElement<T>(this IReadOnlyList<T> list)
        {
            if (list.Count == 0)
                return default;
            return list[Random.Range(0, list.Count)];
        }

        public static T TakeRandomElement<T>(this IList<T> list)
        {
            if (list.Count == 0)
                return default;
            var index = Random.Range(0, list.Count);
            var res = list[index];
            list.RemoveAt(index);
            return res;
        }
    }
}