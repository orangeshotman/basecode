using UnityEngine;

namespace OrangeShotStudio.Scripts.Tools
{
    public static class ToolsExtensions
    {
        public static bool IsTestLoop()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
            var actionName = intent.Call<string>("getAction");
            Debug.Log(actionName);
            return actionName == "com.google.intent.action.TEST_LOOP";
#else
            return false;
#endif
        }
    }
}