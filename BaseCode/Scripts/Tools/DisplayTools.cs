
namespace OrangeShotStudio.Scripts.Tools
{
    public static class DisplayTools
    {
        public static int DisplayTarget()
        {
            int displayID = 0;


#if UNITY_EDITOR
            var mouseOverWindow = UnityEditor.EditorWindow.mouseOverWindow;
            System.Reflection.Assembly assembly = typeof(UnityEditor.EditorWindow).Assembly;
            var type = assembly.GetType("UnityEditor.PlayModeView");

            var window = UnityEditor.EditorWindow.focusedWindow;
            
            if (type.IsInstanceOfType(window))
            {
                var displayField = type.GetField("m_TargetDisplay", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                displayID = (int)displayField.GetValue(window);
            }
#endif
            return displayID;
        }
    }
}