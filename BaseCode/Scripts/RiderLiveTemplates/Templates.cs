﻿using System.Collections;
using System.Collections.Generic;
using BaseCode.Scripts;
using UnityEngine;

public class Templates : MonoBehaviour
{
    // public class $name$State : BaseGameState
    // {
    //     private readonly InterfaceView _interfaceView;
    //
    //     public $name$State(InterfaceView interfaceView, $ApplicationName$StateFactory factory)
    //     {
    //         _interfaceView = interfaceView;
    //     }
    // }
    //
    // public partial class $ApplicationName$StateFactory : BaseGameStateFactory
    // {
    //     public IGameState Create$name$State()
    //     {
    //         return new $name$State(InterfaceView, this);
    //     }
    // }
    //
    //
    // public class $DataName$Provider : DataProvider<$DbName$, $DataName$>
    // {
    //     public $DataName$Provider() : base("$DbName$")
    //     {
    //     }
    // }
    //
    // [CreateAssetMenu(fileName = "$DbName$")]
    // public class $DbName$ : BaseDb<$DataName$>
    // {
    //         
    // }
    //
    // [Serializable]
    // public class $DataName$ : IDefinition
    // {
    //     [SerializeField]
    //     private string _id;
    //     public string Id => _id;
    // }
    //
    //
    // public class $ApplicationName$Launcher : BaseLauncher
    // {
    //     private readonly ICompositionRoot _cr = new $ApplicationName$CompositionRoot();
    //
    //     protected override ICompositionRoot Cr => _cr;
    // }
    //
    // public class $ApplicationName$CompositionRoot : ICompositionRoot
    // {
    //     public void Launch()
    //     {
    //         var prefabProvider = new PrefabProvider();
    //         var windowsFactory = new $ApplicationName$WindowsFactory(prefabProvider);
    //         var interfaceView = new InterfaceView(prefabProvider, windowsFactory);
    //         var stateFactory = new $ApplicationName$StateFactory(prefabProvider, interfaceView);
    //         var analyticManager = new AnalyticManager();
    //         new $ApplicationName$Controller(stateFactory, interfaceView, analyticManager);
    //     }
    // }
    //
    // public class $ApplicationName$Controller : BaseGameController
    // {
    //     private readonly InterfaceView _interfaceView;
    //     public $ApplicationName$Controller($ApplicationName$StateFactory $ApplicationName$StateFactory, InterfaceView interfaceView, IAnalyticsService analyticsService) : base(analyticsService)
    //     {
    //          _interfaceView = interfaceView;
    //     }
    //     
    //     protected override void PreStateChange()
    //     {
    //         _interfaceView.HideAll();
    //     }
    // }
    //
    // public class $ApplicationName$WindowsFactory : IWindowsFactory
    // {
    //     private readonly IPrefabProvider _prefabProvider;
    //
    //     public $ApplicationName$WindowsFactory(IPrefabProvider prefabProvider)
    //     {
    //         _prefabProvider = prefabProvider;
    //     }        
    // }
    //
    // public partial class $ApplicationName$StateFactory : BaseGameStateFactory
    // {
    //     public $ApplicationName$StateFactory(IPrefabProvider prefabProvider, InterfaceView interfaceView) : base(prefabProvider, interfaceView)
    //     {
    //     }        
    // }
    //
    //
    // public class $WindowName$Window : BaseWindow<$WindowName$Widget>, I$WindowName$Window
    // {
    //     public $WindowName$Window(IPrefabProvider prefabProvider) : base(prefabProvider, "$WindowName$Window")
    //     {
    //     }
    // }
    //
    // public interface I$WindowName$Window : IWindow
    // {
    //         
    // }
    //
    // public class $WindowName$Widget : MonoBehaviour
    // {
    //         
    // }
    //
    // public class GameplayState : BaseGameState
    // {
    //     private readonly InterfaceView _interfaceView;
    //
    //     public GameplayState(InterfaceView interfaceView, $ApplicationName$StateFactory factory)
    //     {
    //         _interfaceView = interfaceView;
    //     }
    // }
    //
    // public partial class $ApplicationName$StateFactory : BaseGameStateFactory
    // {
    //     public IGameState CreateGameplayState()
    //     {
    //         return new GameplayState(InterfaceView, this);
    //     }
    // }
    //
    // public class MainMenuState : BaseGameState
    // {
    //     private readonly InterfaceView _interfaceView;
    //
    //     public MainMenuState(InterfaceView interfaceView, $ApplicationName$StateFactory factory)
    //     {
    //         _interfaceView = interfaceView;
    //     }
    // }
    //
    // public partial class $ApplicationName$StateFactory : BaseGameStateFactory
    // {
    //     public IGameState CreateMainMenuState()
    //     {
    //         return new MainMenuState(InterfaceView, this);
    //     }
    // }
    //
    // public class ResultState : BaseGameState
    // {
    //     private readonly InterfaceView _interfaceView;
    //
    //     public ResultState(InterfaceView interfaceView, $ApplicationName$StateFactory factory)
    //     {
    //         _interfaceView = interfaceView;
    //     }
    // }
    //
    // public partial class $ApplicationName$StateFactory : BaseGameStateFactory
    // {
    //     public IGameState CreateResultState()
    //     {
    //         return new ResultState(InterfaceView, this);
    //     }
    // }
}
