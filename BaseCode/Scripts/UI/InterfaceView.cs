﻿using System;
using System.Collections.Generic;
using OrangeShotStudio.Behaviours;
using OrangeShotStudio.Provider;
using OrangeShotStudio.Structuries;
using UnityEngine;
using Object = UnityEngine.Object;

namespace OrangeShotStudio.View
{
    public class InterfaceView : IDisposable
    {
        private readonly Transform _root;
        private readonly List<IWindow> _windows;

        public InterfaceView(IPrefabProvider prefabProvider, IWindowsFactory factory)
        {
            _root = Object.Instantiate(prefabProvider.GetCanvasPrefab).transform;
            Object.DontDestroyOnLoad(_root);
            _windows = factory.CreateAllWindows();
            foreach (var window in _windows)
            {
                window.Initialize(_root);
            }
            HideAll();
        }

        public T GetWindow<T>() where T : class, IWindow
        {
            for (int index = 0; index < _windows.Count; index++)
            {
                var window = _windows[index];
                if (window is T)
                    return window as T;
            }
            return null;
        }

        public void Update(TimeData timeData)
        {
        }

        public void HideAll()
        {
            foreach (var window in _windows)
            {
                window.Hide();
            }   
        }

        public void Dispose()
        {

        }
    }
}