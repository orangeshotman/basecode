using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;

public class UIAnimatedGraphicsLine : Graphic
{
    public int VertexCount = 1000;
    public float Range;
    public float Width = 1;
    public float Min;
    public float Max;
    public float UpdateFrequency;
    private float _lastUpdate;
    private bool _initialized;
    private List<UIVertex> _uiVertices;
    private List<int> _indices;
    private UIVertex[] _uiPoints;
    private UIVertex[] _uiCopy;
    private Vector3[] _points;
    private Vector3[] _copy;

    protected override void Awake()
    {
        if (!UnityEngine.Application.isPlaying)
            return;
        _uiPoints = new UIVertex[VertexCount * 2];
        _uiCopy = new UIVertex[VertexCount * 2];
        _uiVertices = new List<UIVertex>(new UIVertex[VertexCount * 2]);
        _indices = new List<int>(new int[(VertexCount - 1) * 6]);
        _points = new Vector3[VertexCount];
        _copy = new Vector3[VertexCount];
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        if (!UnityEngine.Application.isPlaying)
            return;
        Profiler.BeginSample("begin");
        vh.Clear();
        Vector3 point = Vector2.zero;
        Vector3 nextPoint = Vector2.zero;
        Vector3 perpendicular = Vector2.zero;
        var xOffset = rectTransform.pivot.x;
        var yOffset = rectTransform.pivot.y;
        var width = rectTransform.rect.width;
        var height = rectTransform.rect.height;
        var aspect = height / width;
        Profiler.EndSample();
        Profiler.BeginSample("loop");
        for (var index = 0; index < _points.Length; index++)
        {
            Profiler.BeginSample("_indices1");
            if (!_initialized && index != 0)
            {
                _indices[index * 6 - 3] = 2 * index - 1;
                _indices[index * 6 - 2] = 2 * index;
                _indices[index * 6 - 1] = 2 * index + 1;
            }

            Profiler.EndSample();
/*
                Profiler.BeginSample("color");
                point = _points[index];
                if (Mathf.Abs(point.x) < Mathf.Epsilon)
                {
                    // vert.color.a = 0;
                }
                else
                {
                    // vert.color = color;
                }

                Profiler.EndSample();

                Profiler.BeginSample("perpendicular");

                if (index != _points.Length - 1)
                {
                    nextPoint = _points[index + 1];
                    var dir = (nextPoint - point); //.normalized;
                    if (index != 0)
                    {
                        Vector3 previousPoint = _points[index - 1];
                        dir = dir + (point - previousPoint); //.normalized;
                    }

                    perpendicular = Vector2.Perpendicular(dir).normalized * Width;
                    perpendicular.x *= aspect;
                    // perpendicular.y /= _aspect;
                }

                Profiler.EndSample();

                Profiler.BeginSample("set position");

                Profiler.BeginSample("calc");
                var position = point + perpendicular;
                Profiler.EndSample();
                Profiler.BeginSample("scaling");
                position = ScaleByRect(position, width, height, xOffset, yOffset);
                Profiler.EndSample();
                Profiler.BeginSample("setting");
                _uiVertexArray[2 * index].position = position;
                Profiler.EndSample();
                position = point - perpendicular;
                position = ScaleByRect(position, width, height, xOffset, yOffset);
                _uiVertexArray[2 * index + 1].position = position;
                Profiler.EndSample();
                */
            Profiler.BeginSample("_indices2");
            if (!_initialized && index != _points.Length - 1)
            {
                _indices[index * 6] = 2 * index;
                _indices[index * 6 + 1] = 2 * index + 2;
                _indices[index * 6 + 2] = 2 * index + 1;
            }

            Profiler.EndSample();
        }

        Profiler.EndSample();

        Profiler.BeginSample("end");
        _uiVertices.Clear();
        _uiVertices.AddRange(_uiPoints);
        vh.AddUIVertexStream(_uiVertices, _indices);
        _initialized = true;
        Profiler.EndSample();
    }

    public void AddValue(float value)
    {
        Profiler.BeginSample("AddValue");
        var xOffset = rectTransform.pivot.x;
        var yOffset = rectTransform.pivot.y;
        var width = rectTransform.rect.width;
        var height = rectTransform.rect.height;
        var aspect = height / width;
        Rescale(value);
        Array.Copy(_points, 1, _copy, 0, _points.Length - 1);
        Array.Copy(_uiPoints, 2, _uiCopy, 0, _uiPoints.Length - 2);
        var cache = _points;
        _points = _copy;
        _copy = cache;
        var uiCache = _uiPoints;
        _uiPoints = _uiCopy;
        _uiCopy = uiCache;
        var newPoint = new Vector3(1, Mathf.InverseLerp(Min, Max, value));
        _points[_points.Length - 1] = newPoint;
        Vector2 dir = newPoint - _points[_points.Length - 2];
        Vector3 normalizedPerpendicular = Vector2.Perpendicular(dir).normalized;
        RescaleUiPoint(_points.Length - 1, newPoint, aspect, width, height, xOffset, yOffset, normalizedPerpendicular);
        _uiPoints[_uiPoints.Length - 2].color = color;
        _uiPoints[_uiPoints.Length - 1].color = color;
        for (var index = 1; index < _points.Length; index++)
        {
            Vector2 point = _points[index];
            Vector2 prev = _points[index - 1];
            dir = (point - prev).normalized;
            if (index != _points.Length - 1)
            {
                Vector2 next = _points[index + 1];
                dir += (next - point).normalized;
            }

            RescaleUiPoint(index, point, aspect, width, height, xOffset, yOffset, Vector2.Perpendicular(dir).normalized);
        }

        Profiler.EndSample();
    }

    protected void Update()
    {
        if (!UnityEngine.Application.isPlaying)
            return;
        if (Time.time - _lastUpdate < UpdateFrequency)
            return;
        var delta = (Time.time - _lastUpdate) / Range;
        _lastUpdate = Time.time;
        var xOffset = rectTransform.pivot.x;
        var width = rectTransform.rect.width;
        for (int i = 0; i < _points.Length; i++)
        {
            var x = _points[i].x;
            x = Mathf.Max(0, x - delta);
            _points[i].x = x;
            x = _uiPoints[i * 2].position.x;
            x = NormalizeByRect(x, width, xOffset);
            x = Mathf.Max(0, x - delta);
            x = ScaleAxisByRect(x, width, xOffset);
            _uiPoints[i * 2].position.x = x;
            x = _uiPoints[i * 2 + 1].position.x;
            x = NormalizeByRect(x, width, xOffset);
            x = Mathf.Max(0, x - delta);
            x = ScaleAxisByRect(x, width, xOffset);
            _uiPoints[i * 2 + 1].position.x = x;
        }

        SetVerticesDirty();
    }

    private void Rescale(float newValue)
    {
        var xOffset = rectTransform.pivot.x;
        var yOffset = rectTransform.pivot.y;
        var width = rectTransform.rect.width;
        var height = rectTransform.rect.height;
        var aspect = height / width;
        var newMax = Single.MinValue;
        var newMin = Single.MaxValue;
        for (var index = 1; index < _points.Length; index++)
        {
            var point = _points[index];
            var value = Mathf.Lerp(Min, Max, point.y);
            newMax = Mathf.Max(newMax, value);
            newMin = Mathf.Min(newMin, value);
        }

        newMax = Mathf.Max(newMax, newValue);
        newMin = Mathf.Min(newMin, newValue);
        for (var index = 1; index < _points.Length; index++)
        {
            var point = _points[index];
            var value = Mathf.Lerp(Min, Max, point.y);
            point.y = Mathf.InverseLerp(newMin, newMax, value);
            _points[index] = point;
        }

        Min = newMin;
        Max = newMax;
    }

    private Vector3 ScaleByRect(Vector3 point, float width, float height, float xOffset, float yOffset)
    {
        point.x -= xOffset;
        point.y -= yOffset;
        point.x *= width;
        point.y *= height;
        return point;
    }

    private float ScaleAxisByRect(float point, float range, float offset)
    {
        point -= offset;
        point *= range;
        return point;
    }

    private float NormalizeByRect(float point, float range, float offset)
    {
        point /= range;
        point += offset;
        return point;
    }

    private void RescaleUiPoint(int index, Vector3 value, float aspect, float width, float height, float xOffset, float yOffset, Vector3 normalizedPerpendicular)
    {
        Vector3 perpendicular = normalizedPerpendicular * Width;
        perpendicular.x *= aspect;
        var point = value + perpendicular;
        _uiPoints[index * 2].position = ScaleByRect(point, width, height, xOffset, yOffset);
        point = value - perpendicular;
        _uiPoints[index * 2 + 1].position = ScaleByRect(point, width, height, xOffset, yOffset);
    }
}