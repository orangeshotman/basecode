using System;
using TMPro;
using UnityEngine;

public class UiAnimatedGraphics : MonoBehaviour
{
    public UIAnimatedGraphicsLine Line;
    public TextMeshProUGUI MaxValue;
    public TextMeshProUGUI MinValue;
    private float _maxValue;
    private float _minValue;

    public void AddValue(float val)
    {
        Line.AddValue(val);
    }

    private void Update()
    {
        if (Math.Abs(_maxValue - Line.Max) > 0.0001f)
        {
            _maxValue = Line.Max;
            MaxValue.text = $"{Line.Max:0.#}";
        }

        if (Math.Abs(_minValue - Line.Min) > 0.0001f)
        {
            _minValue = Line.Min;
            MinValue.text = $"{Line.Min:0.#}";
        }
    }
}