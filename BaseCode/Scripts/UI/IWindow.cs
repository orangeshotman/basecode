﻿using UnityEngine;

namespace OrangeShotStudio.View
{
    public interface IWindow
    {
        void Initialize(Transform root);
        void Show();
        void Hide();
    }
}