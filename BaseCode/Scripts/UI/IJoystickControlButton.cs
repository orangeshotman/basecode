using UnityEngine;

namespace OrangeShotStudio.View.UI
{
    public interface IJoystickControlButton : IPointControlButton
    {
        Vector2 CurrentDirection { get; }
    }
}