using UnityEngine;

namespace OrangeShotStudio.View.UI
{
    public interface IDragControlButton : IPointControlButton
    {
        bool Released { get; }
        Vector2 Delta { get; }
    }
}