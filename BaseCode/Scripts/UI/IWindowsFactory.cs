﻿using System.Collections.Generic;

namespace OrangeShotStudio.View
{
    public interface IWindowsFactory
    {
        List<IWindow> CreateAllWindows();
    }
}