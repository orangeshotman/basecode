using UnityEngine;

namespace OrangeShotStudio.View.UI
{
    public interface IPointControlButton : IControllButton
    {
        Vector2 Position { get; }
    }
}