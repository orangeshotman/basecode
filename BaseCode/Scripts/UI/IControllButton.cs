﻿namespace OrangeShotStudio.View.UI
{
    public interface IControllButton
    {
        bool Pushed { get; }
    }
}