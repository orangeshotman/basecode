using System;
using UnityEngine;

namespace OrangeShotStudio.View
{
    public interface IUi3DObjectsView : IDisposable
    {
        RenderTexture RenderTexture { get; }
        void SetRenderObject(GameObject prefab);
        void RotateObjectX(float angle);
    }
}