﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace OrangeShotStudio.View.UI
{
    public class ClickControllButton : MonoBehaviour, IPointerClickHandler, IControllButton
    {
        public bool Pushed { get { return _clickFrame == Time.frameCount; } }
        private int _clickFrame = -1;
        public void OnPointerClick(PointerEventData eventData)
        {
            _clickFrame = Time.frameCount;
        }
    }
}