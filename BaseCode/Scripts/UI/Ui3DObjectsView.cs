using OrangeShotStudio.Provider;
using UnityEngine;
using Object = UnityEngine.Object;

namespace OrangeShotStudio.View
{
    public class Ui3DObjectsView : IUi3DObjectsView
    {
        private readonly Vector3 _offset;
        private readonly Camera _objectCamera;
        private readonly RenderTexture _renderTexture;
        public GameObject RenderObject;

        public RenderTexture RenderTexture
        {
            get { return _renderTexture; }
        }

        public Ui3DObjectsView(IPrefabProvider prefabProvider, RectTransform rawImageRect, int resolution,
            Vector3 offset) : this(Object.Instantiate(prefabProvider.GetPrefab("Ui3dCamera")).GetComponent<Camera>(),
            rawImageRect, resolution, offset)
        {
        }

        public Ui3DObjectsView(Camera camera, RectTransform rawImageRect, int resolution, Vector3 offset)
        {
            _offset = offset;
            var aspect = rawImageRect.rect.width / rawImageRect.rect.height;
            _renderTexture = new RenderTexture(resolution, (int)(resolution / aspect), 16);
            _objectCamera = camera;
            _objectCamera.targetTexture = _renderTexture;
            _objectCamera.transform.position += offset;
        }

        public Ui3DObjectsView(IPrefabProvider prefabProvider, Vector3 offset, int width = 700, int height = 1024)
        {
            _offset = offset;
            var go = Object.Instantiate(prefabProvider.GetPrefab("Ui3dCamera"));
            go.transform.position += offset;
            _renderTexture = new RenderTexture(width, height, 16);
            _objectCamera = go.GetComponent<Camera>();
            _objectCamera.targetTexture = _renderTexture;
        }

        public void SetRenderObject(GameObject prefab)
        {
            if (RenderObject)
                Object.Destroy(RenderObject);
            RenderObject = Object.Instantiate(prefab);
            RenderObject.layer = 14;
            ChangeLayersRecursively(RenderObject.transform, 14);
            RenderObject.transform.position += _offset;
        }

        public void SetRenderObject(GameObject prefab, Vector2 position)
        {
            if (RenderObject)
                Object.Destroy(RenderObject);
            RenderObject = Object.Instantiate(prefab, position, Quaternion.identity);
            RenderObject.layer = 14;
            ChangeLayersRecursively(RenderObject.transform, 14);
            RenderObject.transform.position += _offset;
        }

        public void RotateObjectX(float angle)
        {
            if (RenderObject == null)
                return;

            RenderObject.transform.Rotate(Vector3.up, angle);
        }

        public void SetBackgroundColor(Color clr)
        {
            _objectCamera.backgroundColor = clr;
        }

        public void Dispose()
        {
            Object.Destroy(_objectCamera.gameObject);
            Object.Destroy(_renderTexture);
            Object.Destroy(RenderObject);
        }

        private void ChangeLayersRecursively(Transform trans, int layer)
        {
            trans.gameObject.layer = layer;
            foreach (Transform child in trans)
            {
                ChangeLayersRecursively(child, layer);
            }
        }
    }
}