using UnityEngine;
using UnityEngine.EventSystems;

namespace OrangeShotStudio.View.UI
{
    public class MoveControlButton : MonoBehaviour, IPointControlButton, IPointerDownHandler, IPointerUpHandler
    {
        public bool Pushed { get; private set; }
        public Vector2 Position { get; private set; }

        public void OnPointerDown(PointerEventData eventData)
        {
            Pushed = true;
            Position = Input.mousePosition;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Pushed = false;
        }

        void Update()
        {
            if(Pushed)
                Position = Input.mousePosition;
        }

        void OnDisable()
        {
            Pushed = false;
        }
    }
}