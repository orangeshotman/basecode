using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace OrangeShotStudio.View.UI
{
    public class DragControlButton : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler,
        IEndDragHandler, IDragControlButton, IPointerUpHandler
    {
        private int _onPrePushingFrame;
        private int _onBeginDragFrame;
        private int _onEndDragFrame;
        private int _lastActionFrame;
        private Vector2 _delta;
        public bool PrePushed => _onPrePushingFrame <= Time.frameCount;
        public bool Pushed => _onBeginDragFrame <= Time.frameCount;
        public bool Released => _onEndDragFrame == Time.frameCount;
        public Vector2 Position { get; private set; }

        public Vector2 Delta
        {
            get => Time.frameCount == _lastActionFrame ? _delta : Vector2.zero;
            private set => _delta = value;
        }

        public List<RaycastResult> LastHoveredOnEndDrag { get; } = new List<RaycastResult>();

        private void OnEnable()
        {
            _onBeginDragFrame = Int32.MaxValue;
            _onPrePushingFrame = Int32.MaxValue;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            Position = eventData.position;
            Delta = eventData.delta;
            _onBeginDragFrame = Time.frameCount;
            _lastActionFrame = Time.frameCount;
        }

        public void OnDrag(PointerEventData eventData)
        {
            Position = eventData.position;
            Delta = eventData.delta;
            _lastActionFrame = Time.frameCount + 1;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Position = eventData.position;
            Delta = eventData.delta;
            _onBeginDragFrame = Int32.MaxValue;
            _onEndDragFrame = Time.frameCount;
            _lastActionFrame = Time.frameCount;
            _onPrePushingFrame = Int32.MaxValue;
            EventSystem.current.RaycastAll(eventData, LastHoveredOnEndDrag);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _onPrePushingFrame = Time.frameCount;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _onPrePushingFrame = Int32.MaxValue;
        }
    }
}