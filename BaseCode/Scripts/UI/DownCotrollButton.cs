﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace OrangeShotStudio.View.UI
{
    public class DownCotrollButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IControllButton
    {
        public bool Pushed { get; private set; }

        public void OnPointerDown(PointerEventData eventData)
        {
            Pushed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Pushed = false;
        }

        private void OnDisable()
        {
            Pushed = false;
        }
    }
}