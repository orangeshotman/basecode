using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace OrangeShotStudio.View.UI
{
    public class JoystickControlButton : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,
        IJoystickControlButton
    {
        public Image Background;
        public Image Stick;
        public float _MaxMagnitude = 1;
        public bool HideJoystick;
        public float MaxMagnitude
        {
            get { return _MaxMagnitude * Screen.dpi; }
        }

        private Vector2 _currentDirection;
        public bool Pushed { get; private set; }
        public Vector2 Position { get; private set; }
        public Vector2 CurrentDirection => _currentDirection / MaxMagnitude;

        public void OnBeginDrag(PointerEventData eventData)
        {
            Position = eventData.position;
            Background.rectTransform.position = Position;
            Stick.rectTransform.position = Position;
            Pushed = true;
            _currentDirection = Vector2.zero;
            if (HideJoystick)
            {
                Background.gameObject.SetActive(true);
                Stick.gameObject.SetActive(true);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            _currentDirection += eventData.delta;
            Position = eventData.position;
            Stick.rectTransform.position = Position;
            Background.rectTransform.position = Position + (-_currentDirection);
            _currentDirection = Vector2.ClampMagnitude(_currentDirection, MaxMagnitude);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Position = eventData.position;
            Pushed = false;
            Stick.rectTransform.position = Position + (-_currentDirection);
            Background.rectTransform.position = Position + (-_currentDirection);
            if (HideJoystick)
            {
                Background.gameObject.SetActive(false);
                Stick.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            if (HideJoystick)
            {
                Background.gameObject.SetActive(false);
                Stick.gameObject.SetActive(false);
            }
        }

        private void OnDisable()
        {
            Pushed = false;
            Stick.rectTransform.position = Position + (-_currentDirection);
            Background.rectTransform.position = Position + (-_currentDirection);
        }
    }
}