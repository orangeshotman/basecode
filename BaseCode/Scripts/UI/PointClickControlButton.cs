using UnityEngine;
using UnityEngine.EventSystems;

namespace OrangeShotStudio.View.UI
{
    public class PointClickControlButton : MonoBehaviour, IPointerClickHandler, IPointControlButton
    {
        public bool Pushed { get { return _clickFrame == Time.frameCount; } }
        public Vector2 Position { get { return _position; } }
        private int _clickFrame;
        private Vector2 _position;
        public void OnPointerClick(PointerEventData eventData)
        {
            _clickFrame = Time.frameCount;
            _position = eventData.position;
        }
    }
}