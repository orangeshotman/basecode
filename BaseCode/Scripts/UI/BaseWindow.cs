using OrangeShotStudio.Provider;
using OrangeShotStudio.View;
using UnityEngine;
using Object = UnityEngine.Object;

namespace OrangeShotStudio.CarDrift.View.UI
{
    public abstract class BaseWindow<T> : IWindow  where T : MonoBehaviour
    {
        protected T Widget;
        private readonly IPrefabProvider _prefabProvider;
        private readonly string _prefabName;

        protected BaseWindow(IPrefabProvider prefabProvider, string prefabName)
        {
            _prefabProvider = prefabProvider;
            _prefabName = prefabName;
        }

        public virtual void Initialize(Transform root)
        {
            Widget = Object.Instantiate(_prefabProvider.GetPrefab(_prefabName), root, false).GetComponent<T>();
        }

        public virtual void Show()
        {
            Widget.gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            Widget.gameObject.SetActive(false);
        }
    }
}