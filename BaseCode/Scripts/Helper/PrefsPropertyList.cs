using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrangeShotStudio.Data
{
    [Serializable]
    public class ListWrapper<T>
    {
        public List<T> List;
        public ListWrapper(List<T> list)
        {
            List = list;
        } 
    }
    public class PrefsPropertyList<T>
    {
        private readonly string _key;
        private readonly List<T> _defaultValue;
        private bool _hasNewData;
        private ListWrapper<T> _cache; 
        public PrefsPropertyList(string key, List<T> defaultValue)
        {
            _defaultValue = defaultValue;
            _key = key;
            _hasNewData = true;
        }

        public List<T> List 
        {
            get
            {
                if (!PlayerPrefs.HasKey(_key))
                {
                    PlayerPrefs.SetString(_key, JsonUtility.ToJson(new ListWrapper<T>(_defaultValue)));
                    PlayerPrefs.Save();
                    _hasNewData = true;
                }
                if (_hasNewData)
                {
                    _cache = JsonUtility.FromJson<ListWrapper<T>>(PlayerPrefs.GetString(_key));
                    _hasNewData = false;
                }
                return new List<T>(_cache.List);
            }
            set
            {
                PlayerPrefs.SetString(_key, JsonUtility.ToJson(new ListWrapper<T>(value)));
                PlayerPrefs.Save();
                _hasNewData = true;
            }
        }
    }
}