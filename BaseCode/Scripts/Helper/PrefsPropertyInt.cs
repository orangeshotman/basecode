using UnityEngine;

namespace OrangeShotStudio.Data
{
    public class PrefsPropertyInt
    {
        private readonly string _key;
        private readonly int _defaulValue;

        public PrefsPropertyInt(string key, int defaulValue)
        {
            _key = key;
            _defaulValue = defaulValue;
        }

        public int Value
        {
            get
            {
                if (!PlayerPrefs.HasKey(_key))
                {
                    PlayerPrefs.SetInt(_key, _defaulValue);
                    PlayerPrefs.Save();
                }
                return PlayerPrefs.GetInt(_key);
            }
            set
            {
                PlayerPrefs.SetInt(_key, value);
                PlayerPrefs.Save();
            }
        }
    }
}