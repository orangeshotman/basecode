using System;
using Newtonsoft.Json;
using UnityEngine;

namespace OrangeShotStudio.Data
{
    public class PrefsPropertyObject<T> where T : new()
    {
        private PrefsPropertyString _savedData;
        private string _propertyKey;

        public T Value
        {
            get
            {
                var json = _savedData.Value;
                try
                {
                    var deserializeObject = JsonConvert.DeserializeObject<T>(json);
                    if (deserializeObject == null)
                    {
                        deserializeObject = ResetData();
                    }

                    return deserializeObject;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    return ResetData();
                }
            }
            set
            {
                var json = JsonConvert.SerializeObject(value);
                _savedData.Value = json;
            }
        }

        public PrefsPropertyObject(string propertyKey)
        {
            _propertyKey = propertyKey;
            CreateData();
        }

        private void CreateData()
        {
            var userData = new T();
            var defaultData = JsonConvert.SerializeObject(userData);
            _savedData = new PrefsPropertyString(_propertyKey, defaultData);
        }

        private T ResetData()
        {
            PlayerPrefs.DeleteKey(_propertyKey);
            CreateData();
            var json = _savedData.Value;
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}