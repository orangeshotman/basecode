using UnityEngine;

namespace OrangeShotStudio.Data
{
    public class PrefsPropertyFloat
    {
        private readonly string _key;
        private readonly float _defaulValue;

        public PrefsPropertyFloat(string key, float defaulValue)
        {
            _key = key;
            _defaulValue = defaulValue;
        }

        public float Value
        {
            get
            {
                if (!PlayerPrefs.HasKey(_key))
                {
                    PlayerPrefs.SetFloat(_key, _defaulValue);
                    PlayerPrefs.Save();
                }
                return PlayerPrefs.GetFloat(_key);
            }
            set
            {
                PlayerPrefs.SetFloat(_key, value);
                PlayerPrefs.Save();
            }
        }
    }
}