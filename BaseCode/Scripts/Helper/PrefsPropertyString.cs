using UnityEngine;

namespace OrangeShotStudio.Data
{
    public class PrefsPropertyString
    {
        private readonly string _key;
        private readonly string _defaulValue;

        public PrefsPropertyString(string key, string defaulValue)
        {
            _key = key;
            _defaulValue = defaulValue;
        }

        public string Value
        {
            get
            {
                if (!PlayerPrefs.HasKey(_key))
                {
                    PlayerPrefs.SetString(_key, _defaulValue);
                    PlayerPrefs.Save();
                }

                return PlayerPrefs.GetString(_key);
            }
            set
            {
                PlayerPrefs.SetString(_key, value);
                PlayerPrefs.Save();
            }
        }
    }
}