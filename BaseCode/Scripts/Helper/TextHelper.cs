using System;

namespace OrangeShotStudio.Data
{
    public static class TextHelper
    {
        private static readonly char[] IncPrefixes = { 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y' };
        private static readonly char[] DecPrefixes = { 'm', '\u03bc', 'n', 'p', 'f', 'a', 'z', 'y' };

        public static string ToSi(this int d, string format = null)
        {
            if (d == 0)
                return "0";
            int degree = (int)Math.Floor(Math.Log10(Math.Abs(d)) / 3);
            double scaled = d * Math.Pow(1000, -degree);

            char? prefix = null;
            switch (Math.Sign(degree))
            {
                case 1: prefix = IncPrefixes[degree - 1]; break;
                case -1: prefix = DecPrefixes[-degree - 1]; break;
            }

            return scaled.ToString(format) + prefix;
        }
    }
}