using UnityEngine;

namespace OrangeShotStudio.Data
{
    public class PrefsPropertyBool
    {
        private readonly string _key;
        private readonly bool _defaultValue;

        public PrefsPropertyBool(string key, bool defaultValue)
        {
            _key = key;
            _defaultValue = defaultValue;
        }

        public bool Value
        {
            get
            {
                if (!PlayerPrefs.HasKey(_key))
                {
                    PlayerPrefs.SetInt(_key, _defaultValue ? 1 : 0);
                    PlayerPrefs.Save();
                }
                return PlayerPrefs.GetInt(_key) == 1;
            }
            set
            {
                PlayerPrefs.SetInt(_key, value ? 1 : 0);
                PlayerPrefs.Save();
            }
        }
    }
}