using OrangeShotStudio.Structuries;
using UnityEngine;

namespace Assets.Scripts.Model
{
    public class Multiplier
    {
        private readonly float _maxWaitTime;
        private float _lastEventTime;

        public Multiplier(float maxWaitTime)
        {
            _maxWaitTime = maxWaitTime;
        }

        public int MultiplierValue { get; private set; }
        public void ProcessEvent(TimeData timeData)
        {
            _lastEventTime = timeData.TimeSinceStartUp;
            MultiplierValue++;
        }

        public void Update(TimeData timeData)
        {
            if (timeData.TimeSinceStartUp > _lastEventTime + _maxWaitTime && MultiplierValue != 0)
                MultiplierValue = 0;
        }
    }
}