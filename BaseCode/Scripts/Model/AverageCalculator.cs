using System.Collections.Generic;

namespace Assets.Scripts.Model
{
    public class AverageCalculator
    {
        private readonly Queue<float> _history;
        private readonly int _windowSize;
        private float _sum;
        public float Average => _sum / _history.Count;

        public AverageCalculator(int windowSize)
        {
            _history = new Queue<float>(windowSize + 1);
            _windowSize = windowSize;
            _sum = 0;
        }

        public void Add(float value)
        {
            _sum += value;
            _history.Enqueue(value);
            if (_history.Count > _windowSize)
            {
                _sum -= _history.Dequeue();
            }
        }

    }
}